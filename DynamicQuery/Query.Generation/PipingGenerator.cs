﻿using System;
using System.Linq;
using DynamicQuery.Query.Syntax;

namespace DynamicQuery.Query.Generation
{
    public class PipingGenerator : IGenerator<PipeNode>
    {
        private readonly IGenerator<FilterNode> _filterGenerator;
        private readonly IGenerator<AggregateNode> _aggregationGenerator;
        private readonly IGenerator<SelectNode> _selectGenerator;
        private readonly IGenerator<SortNode> _sortGenerator;

        public PipingGenerator(
            IGenerator<FilterNode> filterGenerator, 
            IGenerator<AggregateNode> aggregationGenerator, 
            IGenerator<SelectNode> selectGenerator, 
            IGenerator<SortNode> sortGenerator)
        {
            _filterGenerator = filterGenerator;
            _aggregationGenerator = aggregationGenerator;
            _selectGenerator = selectGenerator;
            _sortGenerator = sortGenerator;
        }

        public SequenceOperation Generate(Type elementType, PipeNode node)
        {
            var inner = GenerateOperation(elementType, node.Pipings.First());

            foreach (var piping in node.Pipings.Skip(1))
            {
                var previous = inner;
                var outer = GenerateOperation(inner.OutType, piping);
                
                inner = new SequenceOperation(
                    seq => outer.Function(previous.Function(seq)),
                    previous.InType,
                    outer.OutType);
            }

            return inner;
        }

        private SequenceOperation GenerateOperation(Type elementType, IQueryNode piping)
        {
            switch (piping.NodeType)
            {
                case QueryNodeType.Filter:
                    return _filterGenerator.Generate(elementType, (FilterNode) piping);
                case QueryNodeType.Aggregate:
                    return _aggregationGenerator.Generate(elementType, (AggregateNode) piping);
                case QueryNodeType.Select:
                    return _selectGenerator.Generate(elementType, (SelectNode) piping);
                case QueryNodeType.Sort:
                    return _sortGenerator.Generate(elementType, (SortNode) piping);
                case QueryNodeType.Pipe:
                    return Generate(elementType, (PipeNode) piping);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}