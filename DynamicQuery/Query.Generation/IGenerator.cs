﻿using System;
using DynamicQuery.Query.Syntax;

namespace DynamicQuery.Query.Generation
{
    public interface IGenerator<in T> where T : IQueryNode
    {
        SequenceOperation Generate(Type elementType, T node);
    }
}