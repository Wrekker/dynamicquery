﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using DynamicQuery.Query.Syntax;
using DynamicQuery.Types;

namespace DynamicQuery.Query.Generation
{
    public class SortGenerator : IGenerator<SortNode>
    {
        readonly TerminalGenerator _terminalGenerator;

        public SortGenerator(TerminalGenerator terminalGenerator)
        {
            _terminalGenerator = terminalGenerator;
        }

        public SequenceOperation Generate(Type elementType, SortNode node)
        {
            var order = GenerateOrderBy(elementType, node.Orderings.ToArray(), 0, null);

            if (node.Limit != null)
            {
                var limit = GenerateLimit(elementType, node.Limit);

                return new SequenceOperation(
                    q => limit(order(q)),
                    elementType,
                    elementType);
            }

            return new SequenceOperation(
                order,
                elementType,
                elementType);
        }

        private Func<IQueryable, IQueryable> GenerateOrderBy(Type elementType, IReadOnlyList<OrderingNode> stack, int index, Func<IQueryable, IQueryable> inner)
        {
            if (stack == null || stack.Count <= index)
            {
                return inner;
            }

            var orderBy = stack[index];
            var orderProperty = GetPropertyInfo(elementType, orderBy.Member.MemberName);
            var orderAccessor = MakePropertyAccessor(elementType, orderProperty);
            var orderMethod = index == 0
                ? MethodFactory.MakeOrderBy(elementType, orderProperty.PropertyType, orderBy.IsDescending)
                : MethodFactory.MakeThenBy(elementType, orderProperty.PropertyType , orderBy.IsDescending);

            var generator = new Func<IQueryable, Expression>(queryable =>
                Expression.Call(
                    orderMethod,
                    queryable.Expression,
                    Expression.Quote(orderAccessor)));

            return GenerateOrderBy(
                elementType,
                stack,
                index + 1,
                index == 0
                    ? Util.AsGenerator(q => q.Provider.CreateQuery(generator(q)))
                    : Util.AsGenerator(q => q.Provider.CreateQuery(generator(inner(q)))));
        }

        private Func<IQueryable, IQueryable> GenerateLimit(Type elementType, LimitNode node)
        {
            var skipMethod = MethodFactory.MakeSkip(elementType);
            var takeMethod = MethodFactory.MakeTake(elementType);

            var skipParameter = _terminalGenerator.GenerateLiteral(node.Skip);
            var takeParameter = _terminalGenerator.GenerateLiteral(node.Take);

            var generator = new Func<IQueryable, Expression>(queryable =>
                Expression.Call(
                    takeMethod,
                    Expression.Call(
                        skipMethod,
                        queryable.Expression,
                        skipParameter.AsType(typeof(int))),
                    takeParameter.AsType(typeof(int))));

            return Util.AsGenerator(q => q.Provider.CreateQuery(generator(q)));
        }

        private PropertyInfo GetPropertyInfo(Type elementType, string name)
        {
            return elementType.GetProperty(name);
        }

        private Expression MakePropertyAccessor(Type elementType, PropertyInfo propertyInfo)
        {
            var parameter = Expression.Parameter(elementType);

            return Expression.Lambda(
                Expression.Property(
                    parameter,
                    propertyInfo),
                parameter);
        }
    }
}