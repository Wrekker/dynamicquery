﻿using System;
using System.Linq;

namespace DynamicQuery.Query.Generation
{
    public class SequenceOperation
    {
        public Func<IQueryable, IQueryable> Function { get; private set; }
        public Type InType { get; private set; }
        public Type OutType { get; private set; }

        public SequenceOperation(Func<IQueryable, IQueryable> function, Type inType, Type outType)
        {
            Function = function;
            InType = inType;
            OutType = outType;
        }
    }
}