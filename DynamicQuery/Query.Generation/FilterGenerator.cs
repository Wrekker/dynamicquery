﻿using System;
using System.Linq;
using System.Linq.Expressions;
using DynamicQuery.Query.Syntax;
using DynamicQuery.Types;

namespace DynamicQuery.Query.Generation
{
    public class FilterGenerator : IGenerator<FilterNode>
    {
        private readonly LogicalGenerator _logicalGenerator;

        public FilterGenerator(LogicalGenerator logicalGenerator)
        {
            _logicalGenerator = logicalGenerator;
        }

        public SequenceOperation Generate(Type elementType, FilterNode node)
        {
            var parameter = Expression.Parameter(elementType);
            var lambda = Expression.Lambda(_logicalGenerator.GenerateLogical(node.Predicate, parameter), parameter);

            var where = MethodFactory.MakeWhere(elementType);

            var generator = new Func<IQueryable, Expression>(queryable =>
                Expression.Call(
                    where,
                    queryable.Expression,
                    Expression.Quote(lambda)));

            return new SequenceOperation(
                queryable => queryable.Provider.CreateQuery(generator(queryable)),
                elementType,
                elementType);
        }
    }
}