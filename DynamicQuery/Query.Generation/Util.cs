﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DynamicQuery.Query.Generation
{
    public static class Util
    {
        public static Expression Inline(this LambdaExpression self, params Expression[] replacements)
        {
            return Inline(self, (IEnumerable<Expression>)replacements);
        }

        public static Expression Inline(this LambdaExpression self, IEnumerable<Expression> parameterReplacements)
        {
            var rewriter = new ParameterRewriter(self.Parameters, parameterReplacements);

            return rewriter.Visit(self.Body);
        }

        public static Expression AsType(this Expression self, Type targetType)
        {
            if (self.Type != targetType)
            {
                return Expression.Convert(self, targetType);
            }

            return self;
        }
        
        public static Func<IQueryable, IQueryable> AsGenerator(Func<IQueryable, IQueryable> q)
        {
            return q;
        }
    }
}