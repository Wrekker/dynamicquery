using System;
using DynamicQuery.Query.Syntax;

namespace DynamicQuery.Query.Generation
{
    public class QueryGenerator : IGenerator<IQueryNode>
    {
        private readonly IGenerator<FilterNode> _filterGenerator;
        private readonly IGenerator<AggregateNode> _aggregationGenerator;
        private readonly IGenerator<SelectNode> _selectGenerator;
        private readonly IGenerator<SortNode> _sortGenerator;
        private readonly IGenerator<PipeNode> _pipeGenerator;

        public QueryGenerator(
            IGenerator<FilterNode> filterGenerator,
            IGenerator<AggregateNode> aggregationGenerator,
            IGenerator<SelectNode> selectGenerator,
            IGenerator<SortNode> sortGenerator,
            IGenerator<PipeNode> pipeGenerator)
        {
            _filterGenerator = filterGenerator;
            _aggregationGenerator = aggregationGenerator;
            _selectGenerator = selectGenerator;
            _sortGenerator = sortGenerator;
            _pipeGenerator = pipeGenerator;
        }

        public SequenceOperation Generate(Type elementType, IQueryNode node)
        {
            switch (node.NodeType)
            {
                case QueryNodeType.Pipe:
                    return _pipeGenerator.Generate(elementType, (PipeNode)node);
                case QueryNodeType.Filter:
                    return _filterGenerator.Generate(elementType, (FilterNode)node);
                case QueryNodeType.Aggregate:
                    return _aggregationGenerator.Generate(elementType, (AggregateNode)node);
                case QueryNodeType.Select:
                    return _selectGenerator.Generate(elementType, (SelectNode)node);
                case QueryNodeType.Sort:
                    return _sortGenerator.Generate(elementType, (SortNode)node);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}