using System;
using System.Linq.Expressions;
using DynamicQuery.Query.Syntax;
using DynamicQuery.Types;

namespace DynamicQuery.Query.Generation
{
    public class LogicalGenerator
    {
        private readonly TerminalGenerator _terminalGenerator;
        
        public LogicalGenerator(TerminalGenerator terminalGenerator)
        {
            _terminalGenerator = terminalGenerator;
        }

        public Expression GenerateLogical(IQueryNode node, Expression parameter)
        {
            switch (node.NodeType)
            {
                case QueryNodeType.Member:
                    return _terminalGenerator.GenerateMember((MemberNode)node, parameter);
                case QueryNodeType.Literal:
                    return _terminalGenerator.GenerateLiteral((LiteralNode)node);
                default:
                    return GenerateOperation(node, parameter);
            }
        }

        public Expression GenerateOperation(IQueryNode node, Expression parameter)
        {
            switch (node.NodeType)
            {
                case QueryNodeType.Not:
                    return GenerateUnary((UnaryNode)node, parameter);
                case QueryNodeType.Between:
                    return GenerateBetween((BetweenNode)node, parameter);
                default:
                    return GenerateBinary((BinaryNode)node, parameter);
            }
        }

        public Expression GenerateUnary(UnaryNode node, Expression parameter)
        {
            var operand = GenerateLogical(node.Operand, parameter);

            switch (node.NodeType)
            {
                case QueryNodeType.Not:
                    return Expression.Not(operand);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public Expression GenerateBinary(BinaryNode node, Expression parameter)
        {
            var left = GenerateLogical(node.Left, parameter);
            var right = GenerateLogical(node.Right, parameter);

            TypeHelper.CoerceTypes(ref left, ref right);

            switch (node.NodeType)
            {
                case QueryNodeType.Equal:
                    return Expression.Equal(left, right);
                case QueryNodeType.NotEqual:
                    return Expression.NotEqual(left, right);
                case QueryNodeType.LessThan:
                    return Expression.LessThan(left, right);
                case QueryNodeType.LessThanOrEqual:
                    return Expression.LessThanOrEqual(left, right);
                case QueryNodeType.GreaterThan:
                    return Expression.GreaterThan(left, right);
                case QueryNodeType.GreaterThanOrEqual:
                    return Expression.GreaterThanOrEqual(left, right);
                case QueryNodeType.LogicalAnd:
                    return Expression.AndAlso(left, right);
                case QueryNodeType.LogicalOr:
                    return Expression.OrElse(left, right);
                case QueryNodeType.BitwiseAnd:
                    return Expression.And(left, right);
                case QueryNodeType.BitwiseOr:
                    return Expression.Or(left, right);
                case QueryNodeType.StartsWith:
                    return ((Expression<Func<string, string, bool>>)((a, b) => a.StartsWith(b)))
                        .Inline(left, right);
                case QueryNodeType.EndsWith:
                    return ((Expression<Func<string, string, bool>>)((a, b) => a.EndsWith(b)))
                        .Inline(left, right);
                case QueryNodeType.Contains:
                    return ((Expression<Func<string, string, bool>>)((a, b) => a.Contains(b)))
                        .Inline(left, right);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public Expression GenerateBetween(BetweenNode node, Expression parameter)
        {
            var upper = GenerateLogical(node.Upper, parameter);
            var lower = GenerateLogical(node.Lower, parameter);
            var value = GenerateLogical(node.Value, parameter);

            return Expression.AndAlso(
                Expression.GreaterThanOrEqual(value, lower),
                Expression.LessThanOrEqual(value, upper));
        }
    }
}