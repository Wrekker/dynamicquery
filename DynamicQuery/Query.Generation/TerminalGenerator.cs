using System.Linq.Expressions;
using DynamicQuery.Query.Syntax;

namespace DynamicQuery.Query.Generation
{
    public class TerminalGenerator
    {
        public Expression GenerateMember(MemberNode accessNode, Expression parameter)
        {
            return Expression.Property(
                parameter,
                accessNode.MemberName);
        }

        public Expression GenerateLiteral(LiteralNode node)
        {
            return Expression.Constant(node.Value);
        }
    }
}