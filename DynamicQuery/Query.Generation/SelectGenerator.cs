﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using DynamicQuery.Data.Types;
using DynamicQuery.Query.Syntax;
using DynamicQuery.Types;

namespace DynamicQuery.Query.Generation
{
    public class SelectGenerator : IGenerator<SelectNode>
    {
        private readonly TerminalGenerator _terminalGenerator;
        private readonly IDynamicAssemblyBuilder _assemblyBuilder;

        public SelectGenerator(TerminalGenerator terminalGenerator, IDynamicAssemblyBuilder assemblyBuilder)
        {
            _terminalGenerator = terminalGenerator;
            _assemblyBuilder = assemblyBuilder;
        }

        public SequenceOperation Generate(Type elementType, SelectNode node)
        {
            var targetType = MakeAnonymousTargetType(elementType, node.Fields);
            var targetConstructor = targetType.GetConstructor(Type.EmptyTypes);
            var parameter = Expression.Parameter(elementType);

            var select = MethodFactory.MakeSelect(elementType, targetType);
            
            var lambda = Expression.Lambda(
                Expression.MemberInit(
                    Expression.New(targetConstructor),
                    MakeMemberBinds(node.Fields, parameter, targetType)),
                parameter);

            var generator = new Func<IQueryable, Expression>(queryable =>
                Expression.Call(
                    select,
                    queryable.Expression,
                    Expression.Quote(lambda)));

            return new SequenceOperation(
                queryable => queryable.Provider.CreateQuery(generator(queryable)),
                elementType,
                lambda.ReturnType);
        }
        
        private IEnumerable<MemberAssignment> MakeMemberBinds(IReadOnlyList<IQueryNode> members, ParameterExpression parameter, Type targetType)
        {
            foreach (var member in members.OfType<MemberNode>())
            {
                var targetProperty = targetType.GetProperty(member.MemberName);
                var sourceProperty = _terminalGenerator.GenerateMember(member, parameter);

                yield return Expression.Bind(targetProperty, sourceProperty);
            }
        }

        private Type MakeAnonymousTargetType(Type elementType, IReadOnlyList<IQueryNode> members)
        {
            return _assemblyBuilder.CreateType(t =>
            {
                foreach (var member in members.OfType<MemberNode>())
                {
                    var type = elementType.GetProperty(member.MemberName).PropertyType;

                    t.Field(
                        type,
                        member.MemberName,
                        f => f
                            .IsWritable()
                            .IncludeInEquality());
                }

                return t;
            });
        }
    }
}