﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using DynamicQuery.Query.Syntax;
using DynamicQuery.Types;

namespace DynamicQuery.Query.Generation
{
    public class AggregatorGenerator
    {
        public Expression GenerateAggregator(Type sequenceType, AggregatorNode node, ParameterExpression targetParameter)
        {
            var member = node.Member;
            var sourceProperty = sequenceType.GetProperty(member.MemberName);

            MethodInfo sourceAggregatorMethod = null;
            switch (node.NodeType)
            {
                case QueryNodeType.Repeat:
                    return GenerateRepeater(node as RepeatNode);
                case QueryNodeType.Count:
                    return GenerateCount(sequenceType, targetParameter);
                case QueryNodeType.Sum:
                    sourceAggregatorMethod = MethodFactory.MakeSum(sequenceType, sourceProperty.PropertyType);
                    break;
                case QueryNodeType.Average:
                    sourceAggregatorMethod = MethodFactory.MakeAverage(sequenceType, sourceProperty.PropertyType);
                    break;
                case QueryNodeType.Minimum:
                    sourceAggregatorMethod = MethodFactory.MakeMin(sequenceType, sourceProperty.PropertyType);
                    break;
                case QueryNodeType.Maximum:
                    sourceAggregatorMethod = MethodFactory.MakeMax(sequenceType, sourceProperty.PropertyType);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            // a
            var aggregatorParameter = Expression.Parameter(sequenceType);

            // a => a.Prop
            var aggregator = Expression.Lambda(
                Expression.Property(aggregatorParameter, sourceProperty),
                aggregatorParameter);

            // t.Sum(a => a.Prop)
            Expression sourceAggregation = Expression.Call(
                sourceAggregatorMethod,
                targetParameter,
                aggregator);
            
            // (cast)t.Sum(a => a.Prop)
            return sourceAggregation.AsType(sourceProperty.PropertyType);
        }

        public Expression GenerateRepeater(RepeatNode node)
        {
            return Expression.Constant(node.Expression.Value);
        }

        public Expression GenerateCount(Type sequenceType, ParameterExpression targetParameter)
        {
            return Expression.Call(
                MethodFactory.MakeCount(sequenceType),
                targetParameter);
        }
    }
}