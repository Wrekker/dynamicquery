﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using DynamicQuery.Data.Types;
using DynamicQuery.Query.Syntax;
using DynamicQuery.Types;

namespace DynamicQuery.Query.Generation
{
    public class AggregationGenerator : IGenerator<AggregateNode>
    {
        private readonly AggregatorGenerator _aggregatorGenerator;
        private readonly IDynamicAssemblyBuilder _assemblyBuilder;

        public AggregationGenerator(AggregatorGenerator aggregatorGenerator, IDynamicAssemblyBuilder assemblyBuilder)
        {
            _aggregatorGenerator = aggregatorGenerator;
            _assemblyBuilder = assemblyBuilder;
        }

        public SequenceOperation Generate(Type elementType, AggregateNode node)
        {
            var groupByType = MakeGroupByType(elementType, node);
            var groupByResultType = typeof(IGrouping<,>).MakeGenericType(groupByType, elementType);
            var selectResultType = MakeSelectResultType(elementType, node);

            var groupParameter = Expression.Parameter(elementType);
            var selectParameter = Expression.Parameter(groupByResultType);
            
            var groupBy = MethodFactory.MakeGroupBy(elementType, groupByType);
            var select = MethodFactory.MakeSelect(groupByResultType, selectResultType);

            var groupByExpression = MakeGroupBy(node.Over, groupParameter, elementType, groupByType);
            var selectExpression = MakeSelect(node, selectParameter, elementType, groupByType, selectResultType);

            var generator = new Func<IQueryable, Expression>(queryable =>
                Expression.Call(
                    select,
                    Expression.Call(
                        groupBy,
                        queryable.Expression,
                        Expression.Quote(groupByExpression)),
                    Expression.Quote(selectExpression)));
            
            return new SequenceOperation(
                queryable => queryable.Provider.CreateQuery(generator(queryable)),
                elementType,
                selectResultType);
        }

        private Expression MakeGroupBy(OverNode node, ParameterExpression groupParameter, Type sequenceType, Type groupType)
        {
            var constructor = groupType.GetConstructor(Type.EmptyTypes);

            return Expression.Lambda(
                Expression.MemberInit(
                    Expression.New(constructor),
                    MakeGroupBinds(node, groupParameter, sequenceType, groupType)),
                groupParameter);
        }

        private Expression MakeSelect(AggregateNode node, ParameterExpression selectParameter, Type selectType, Type groupByType, Type selectResultType)
        {
            var constructor = selectResultType.GetConstructor(Type.EmptyTypes);

            var overBinds = MakeOverBinds(node.Over, selectParameter, groupByType, selectResultType);
            var aggregatorBinds = MakeAggregatorBinds(node.Aggregators, selectParameter, selectType, selectResultType);
            var binds = overBinds.Concat(aggregatorBinds);

            return Expression.Lambda(
                Expression.MemberInit(
                    Expression.New(constructor),
                    binds),
                selectParameter);
        }

        private IEnumerable<MemberAssignment> MakeGroupBinds(OverNode node, ParameterExpression groupParameter, Type sequenceType, Type groupType)
        {
            foreach (var member in node.Members)
            {
                var sourceProperty = sequenceType.GetProperty(member.MemberName);
                var targetProperty = groupType.GetProperty(member.MemberName);
                
                yield return Expression.Bind(
                    targetProperty,
                    Expression.Property(groupParameter, sourceProperty));
            }
        }

        private IEnumerable<MemberAssignment> MakeOverBinds(OverNode node, ParameterExpression targetParameter, Type groupByType, Type selectResultType)
        {
            foreach (var member in node.Members)
            {
                var targetProperty = selectResultType.GetProperty(member.MemberName);

                var expression = Expression.Property(
                    Expression.Property(targetParameter, "Key"),
                    groupByType.GetProperty(member.MemberName));

                yield return Expression.Bind(
                    targetProperty,
                    expression);
            }
        }

        private IEnumerable<MemberAssignment> MakeAggregatorBinds(IReadOnlyList<AggregatorNode> nodes, ParameterExpression targetParameter, Type selectType, Type selectResultType)
        {
            foreach (var node in nodes)
            {
                var member = node.Member;
                var targetProperty = selectResultType.GetProperty(member.MemberName);

                var expression = _aggregatorGenerator.GenerateAggregator(selectType, node, targetParameter);

                yield return Expression.Bind(
                    targetProperty,
                    expression);
            }
        }

        private Type MakeGroupByType(Type elementType, AggregateNode node)
        {
            return _assemblyBuilder.CreateType(t =>
            {
                foreach (var member in node.Over.Members)
                {
                    var type = elementType.GetProperty(member.MemberName).PropertyType;

                    t.Field(type, member.MemberName, f => f
                        .IsWritable()
                        .IncludeInEquality());
                }

                return t;
            });
        }

        private Type MakeSelectResultType(Type elementType, AggregateNode node)
        {
            return _assemblyBuilder.CreateType(t =>
            {
                foreach (var member in node.Over.Members)
                {
                    var type = elementType.GetProperty(member.MemberName).PropertyType;

                    t.Field(type, member.MemberName, f => f
                        .IsWritable()
                        .IncludeInEquality());
                }

                foreach (var aggregator in node.Aggregators)
                {
                    var type = GetAggregatorType(elementType, aggregator);

                    t.Field(type, aggregator.Member.MemberName, f => f
                        .IsWritable()
                        .IncludeInEquality());
                }

                return t;
            });
        }
        
        private static Type GetAggregatorType(Type elementType, AggregatorNode node)
        {
            switch (node.NodeType)
            {
                case QueryNodeType.Sum:
                case QueryNodeType.Average:
                case QueryNodeType.Minimum:
                case QueryNodeType.Maximum:
                case QueryNodeType.Repeat:
                case QueryNodeType.Over:
                    var propInfo = elementType.GetProperty(node.Member.MemberName);
                    return propInfo.PropertyType;
                case QueryNodeType.Count:
                    return typeof(int);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}