﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace DynamicQuery.Query.Generation
{
    public class ParameterRewriter : ExpressionVisitor
    {
        private Dictionary<ParameterExpression, Expression> _mappings;

        public ParameterRewriter(IEnumerable<ParameterExpression> parameters, IEnumerable<Expression> replacements)
        {
            _mappings = parameters
                .Zip(replacements, Tuple.Create)
                .ToDictionary(k => k.Item1, v => v.Item2);
        }

        protected override Expression VisitParameter(ParameterExpression node)
        {
            return _mappings[node];
        }
    }
}