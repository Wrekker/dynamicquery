﻿using System.Linq;
using System.Reflection;

namespace DynamicQuery.Types
{
    public static class MethodInfoExtensions
    {
        public static T GetDynamically<T>(this object self, string propertyName)
        {
            var property = self.GetType().GetProperty(propertyName);

            return (T)property.GetValue(self);
        }

        public static void SetDynamically(this object self, string propertyName, object value)
        {
            var property = self.GetType().GetProperty(propertyName);

            property.SetValue(self, value);
        }

        public static void InvokeDynamically(this object self, string methodName, params object[] args)
        {
            InvokeDynamically<object>(self, methodName, args);
        }

        public static T InvokeDynamically<T>(this object self, string methodName, params object[] args)
        {
            var types = args.Select(s => s.GetType()).ToArray();
            var method = self.GetType().GetMethod(methodName, types);

            return (T)method.Invoke(null, args);
        }

        public static T InvokeStatic<T>(this MethodInfo method, params object[] args)
        {
            return (T)method.Invoke(null, args);
        }
    }
}