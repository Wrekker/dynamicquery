﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace DynamicQuery.Types
{
    public static class MethodFactory
    {
        public static MethodInfo MakeGroupBy(Type sequenceType, Type keyType)
        {
            return GetQueryableMethod<int>(l => l.GroupBy(a => 0), sequenceType, keyType);
        }
        
        public static MethodInfo MakeSelect(Type sourceType, Type targetType)
        {
            return GetQueryableMethod<int>(l => l.Select(a => 0), sourceType, targetType);
        }

        public static MethodInfo MakeOrderBy(Type sequenceType, Type keyType, bool descending)
        {
            return descending
                ? GetQueryableMethod<int>(l => l.OrderByDescending(a => 0), sequenceType, keyType)
                : GetQueryableMethod<int>(l => l.OrderBy(a => 0), sequenceType, keyType);
        }

        public static MethodInfo MakeThenBy(Type sequenceType, Type keyType, bool descending)
        {
            return descending
                ? GetQueryableMethod<int>(l => l.ThenByDescending(a => 0), sequenceType, keyType)
                : GetQueryableMethod<int>(l => l.ThenBy(a => 0), sequenceType, keyType);
        }

        public static MethodInfo MakeSelectMany(Type sequenceType, Type groupType, Type resultType)
        {
            return GetQueryableMethod<int[]>(l => l.SelectMany(s => s, (r1, r2) => r1), sequenceType, groupType, resultType);
        }

        public static MethodInfo MakeGroupJoin(Type outerType, Type innerType, Type keyType, Type resultType)
        {
            return GetQueryableMethod<int>(q => q.GroupJoin(q, l => l, r => r, (rl, rr) => rl), outerType, innerType, keyType, resultType);
        }

        public static MethodInfo MakeDefaultIfEmpty(Type sequenceType)
        {
            return GetEnumerableMethod<int>(e => e.DefaultIfEmpty(), sequenceType);
        }

        public static MethodInfo MakeSkip(Type sequenceType)
        {
            return GetQueryableMethod<int>(l => l.Skip(0), sequenceType);
        }

        public static MethodInfo MakeTake(Type sequenceType)
        {
            return GetQueryableMethod<int>(l => l.Take(0), sequenceType);
        }

        public static MethodInfo MakeWhere(Type sequenceType)
        {
            return GetQueryableMethod<int>(l => l.Where(a => true), sequenceType);
        }

        public static MethodInfo MakeSum(Type sequenceType, Type resultType)
        {
            var method = GetEnumerableAggregator("Sum", resultType);
            var def = method.GetGenericMethodDefinition();
            
            return def.MakeGenericMethod(sequenceType);
        }

        public static MethodInfo MakeAverage(Type sequenceType, Type resultType)
        {
            var method = GetEnumerableAggregator("Average", resultType);
            var def = method.GetGenericMethodDefinition();
            
            return def.MakeGenericMethod(sequenceType);
        }

        static public MethodInfo MakeMin(Type sequenceType, Type resultType)
        {
            var method = GetEnumerableAggregator("Min", resultType);
            var def = method.GetGenericMethodDefinition();
            
            return def.MakeGenericMethod(sequenceType);
        }

        static public MethodInfo MakeMax(Type sequenceType, Type resultType)
        {
            var method = GetEnumerableAggregator("Max", resultType);
            var def = method.GetGenericMethodDefinition();

            return def.MakeGenericMethod(sequenceType);
        }

        public static MethodInfo MakeCount(Type sequenceType)
        {
            var method = GetEnumerableAggregator("Count");
            var def = method.GetGenericMethodDefinition();

            return def.MakeGenericMethod(sequenceType);
        }

        private static MethodInfo GetEnumerableMethod<TPlaceholder>(Expression<Func<IOrderedEnumerable<TPlaceholder>, IEnumerable<TPlaceholder>>> selector, params Type[] types)
        {
            var def = ((MethodCallExpression)selector.Body).Method.GetGenericMethodDefinition();

            return def.MakeGenericMethod(types);
        }

        private static MethodInfo GetQueryableMethod<TPlaceholder>(Expression<Func<IOrderedQueryable<TPlaceholder>, IQueryable<TPlaceholder>>> selector, params Type[] types)
        {
            var def = ((MethodCallExpression)selector.Body).Method.GetGenericMethodDefinition();

            return def.MakeGenericMethod(types);
        }

        private static MethodInfo GetQueryableMethod<TPlaceholder>(Expression<Func<IOrderedQueryable<TPlaceholder>, IQueryable<IGrouping<TPlaceholder, TPlaceholder>>>> selector, params Type[] types)
        {
            var def = ((MethodCallExpression)selector.Body).Method.GetGenericMethodDefinition();

            return def.MakeGenericMethod(types);
        }
        
        private static MethodInfo GetEnumerableAggregator(string name)
        {
            return typeof(Enumerable).GetMethods().First(w =>
            {
                if (!w.Name.Equals(name)) return false;

                var parameters = w.GetParameters();

                return parameters.Length >= 1;
            });
        }

        private static MethodInfo GetEnumerableAggregator(string name, Type parameterType)
        {
            return typeof (Enumerable).GetMethods().First(w =>
            {
                if (!w.Name.Equals(name)) return false;

                var parameters = w.GetParameters();
                if (parameters.Length < 2) return false;

                var genericTypes = w.GetGenericArguments();
                var lambdaType = typeof (Func<,>).MakeGenericType(genericTypes[0], parameterType);

                return parameters[1].ParameterType == lambdaType;
            });
        }

    }
}