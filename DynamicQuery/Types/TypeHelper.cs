﻿using System;
using System.Collections;
using System.Linq.Expressions;

namespace DynamicQuery.Types
{
    public static class TypeHelper
    {
        public static void CoerceTypes(ref Expression left, ref Expression right)
        {
            if (left.NodeType != ExpressionType.MemberAccess
                && right.NodeType == ExpressionType.MemberAccess)
            {
                // Flip and call 
                CoerceTypes(ref right, ref left);
            }

            // Only left is a member, or neither is.
            right = Expression.Convert(right, left.Type);
        }

        public static Type BoxToNullable(this Type self)
        {
            if (!self.IsValueType)
            {
                return self;
            }

            if (Nullable.GetUnderlyingType(self) != null)
            {
                return self;
            }
            
            return typeof (Nullable<>).MakeGenericType(self);
        }

        public static Expression Convert(this Expression self, Type target)
        {
            if (self.Type == target)
            {
                return self;
            }

            return Expression.Convert(self, target);
        }
    }
}