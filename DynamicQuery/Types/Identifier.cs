﻿using System;
using System.Reflection;

namespace DynamicQuery.Types
{
    public static class Identifier
    {
        private static long _counter = 0;

        public static string Anonymous()
        {
            return "anon" + _counter++;
        }

        public static string DiscriminatedProperty(Type type, PropertyInfo property, string prefix = null)
        {
            return (prefix != null)
                ? string.Format("{0}_{1}_{2}", prefix, type.Name, property.Name)
                : string.Format("{0}_{1}", type.Name, property.Name);
        }
    }
}
