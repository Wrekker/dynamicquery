using System;
using System.Collections.Generic;
using System.Linq;
using DynamicQuery.Query.Syntax;
using Newtonsoft.Json;

namespace DynamicQuery.Query.Parsing
{
    public class JsonQueryDeserializer
    {
        public IQueryNode Deserialize(JsonReader reader)
        {
            reader.Read();
            return Parse(reader);
        }
        
        private IQueryNode Parse(JsonReader reader)
        {
            switch (reader.TokenType)
            {
                case JsonToken.StartArray:
                    return ParseOperation(reader);
                default:
                    return QueryNode.Literal(reader.ReadValue<object>());
            }
        }

        private IQueryNode ParseOperation(JsonReader reader)
        {
            reader.Expect(JsonToken.StartArray);

            var op = reader.ReadValue<string>();
            var nodeType = OperatorToken.MapName(op);
            var ret = ParseOperationInner(reader, nodeType);

            reader.Expect(JsonToken.EndArray);

            return ret;
        }

        private IQueryNode ParseOperationInner(JsonReader reader, QueryNodeType nodeType)
        {
            switch (nodeType)
            {
                case QueryNodeType.Filter:
                    return ParseFilter(nodeType, reader);
                case QueryNodeType.Not:
                case QueryNodeType.Equal:
                case QueryNodeType.NotEqual:
                case QueryNodeType.LessThan:
                case QueryNodeType.LessThanOrEqual:
                case QueryNodeType.GreaterThan:
                case QueryNodeType.GreaterThanOrEqual:
                case QueryNodeType.LogicalAnd:
                case QueryNodeType.LogicalOr:
                case QueryNodeType.BitwiseAnd:
                case QueryNodeType.BitwiseOr:
                case QueryNodeType.Between:
                case QueryNodeType.StartsWith:
                case QueryNodeType.EndsWith:
                case QueryNodeType.Contains:
                    return ParseRelational(nodeType, reader);
                case QueryNodeType.Member:
                    return ParseMember(nodeType, reader);
                case QueryNodeType.Aggregate:
                    return ParseAggregate(nodeType, reader);
                case QueryNodeType.Over:
                    return ParseOver(nodeType, reader);
                case QueryNodeType.Sum:
                case QueryNodeType.Average:
                case QueryNodeType.Minimum:
                case QueryNodeType.Maximum:
                case QueryNodeType.Repeat:
                case QueryNodeType.Count:
                    return ParseAggregator(nodeType, reader);
                case QueryNodeType.Select:
                    return ParseMap(nodeType, reader);
                case QueryNodeType.Sort:
                    return ParseSort(nodeType, reader);
                case QueryNodeType.Ascending:
                case QueryNodeType.Descending:
                    return ParseOrdering(nodeType, reader);
                case QueryNodeType.Limit:
                    return ParseLimit(nodeType, reader);
                case QueryNodeType.Pipe:
                    return ParsePipe(nodeType, reader);
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IReadOnlyList<IQueryNode> ParseList(JsonReader reader)
        {
            var list = new List<IQueryNode>();

            while (reader.TokenType != JsonToken.EndArray)
            {
                list.Add(Parse(reader));   
            }

            return list;
        }

        private IQueryNode ParseFilter(QueryNodeType nodeType, JsonReader reader)
        {
            return QueryNode.Filter(Parse(reader));
        }

        private IQueryNode ParseSort(QueryNodeType nodeType, JsonReader reader)
        {
            var args = ParseList(reader);
            var orderings = args.OfType<OrderingNode>();
            var limit = args.OfType<LimitNode>().SingleOrDefault();

            return QueryNode.Sort(
                orderings,
                limit);
        }

        private IQueryNode ParseOrdering(QueryNodeType nodeType, JsonReader reader)
        {
            switch (nodeType)
            {
                case QueryNodeType.Ascending:
                    return QueryNode.Ascending(Parse(reader));
                case QueryNodeType.Descending:
                    return QueryNode.Descending(Parse(reader));
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IQueryNode ParseLimit(QueryNodeType nodeType, JsonReader reader)
        {
            return QueryNode.Limit(
                Parse(reader),
                Parse(reader));
        }

        private IQueryNode ParseMap(QueryNodeType nodeType, JsonReader reader)
        {
            return QueryNode.Select(ParseList(reader));
        }

        private IQueryNode ParsePipe(QueryNodeType nodeType, JsonReader reader)
        {
            return QueryNode.Pipe(ParseList(reader));
        }
        
        private IQueryNode ParseAggregate(QueryNodeType nodeType, JsonReader reader)
        {
            var args = ParseList(reader);
            var over = args.OfType<OverNode>().Single();
            var members = args.OfType<AggregatorNode>();

            return QueryNode.Aggregate(over, members);
        }

        private IQueryNode ParseOver(QueryNodeType nodeType, JsonReader reader)
        {
            return QueryNode.Over(ParseList(reader));
        }

        private IQueryNode ParseAggregator(QueryNodeType nodeType, JsonReader reader)
        {
            switch (nodeType)
            {
                case QueryNodeType.Sum:
                    return QueryNode.Sum(Parse(reader));
                case QueryNodeType.Average:
                    return QueryNode.Average(Parse(reader));
                case QueryNodeType.Minimum:
                    return QueryNode.Minimum(Parse(reader));
                case QueryNodeType.Maximum:
                    return QueryNode.Maximum(Parse(reader));
                case QueryNodeType.Repeat:
                    return QueryNode.Repeat(Parse(reader), Parse(reader));
                case QueryNodeType.Count:
                    return QueryNode.Count(Parse(reader));
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private IQueryNode ParseMember(QueryNodeType nodeType, JsonReader reader)
        {
            return new MemberNode(reader.ReadValue<string>());
        }

        private IQueryNode ParseRelational(QueryNodeType nodeType, JsonReader reader)
        {
            switch (nodeType)
            {
                case QueryNodeType.Not:
                    return QueryNode.Not(Parse(reader));
                case QueryNodeType.Equal:
                    return QueryNode.Equal(Parse(reader), Parse(reader));
                case QueryNodeType.NotEqual:
                    return QueryNode.NotEqual(Parse(reader), Parse(reader));
                case QueryNodeType.LessThan:
                    return QueryNode.LessThan(Parse(reader), Parse(reader));
                case QueryNodeType.LessThanOrEqual:
                    return QueryNode.LessThanOrEqual(Parse(reader), Parse(reader));
                case QueryNodeType.GreaterThan:
                    return QueryNode.GreaterThan(Parse(reader), Parse(reader));
                case QueryNodeType.GreaterThanOrEqual:
                    return QueryNode.GreaterThanOrEqual(Parse(reader), Parse(reader));
                case QueryNodeType.LogicalAnd:
                    return QueryNode.LogicalAnd(Parse(reader), Parse(reader));
                case QueryNodeType.LogicalOr:
                    return QueryNode.LogicalOr(Parse(reader), Parse(reader));
                case QueryNodeType.BitwiseAnd:
                    return QueryNode.BitwiseAnd(Parse(reader), Parse(reader));
                case QueryNodeType.BitwiseOr:
                    return QueryNode.BitwiseOr(Parse(reader), Parse(reader));
                case QueryNodeType.Between:
                    return QueryNode.Between(Parse(reader), Parse(reader), Parse(reader));
                case QueryNodeType.StartsWith:
                    return QueryNode.StartsWith(Parse(reader), Parse(reader));
                case QueryNodeType.EndsWith:
                    return QueryNode.EndsWith(Parse(reader), Parse(reader));
                case QueryNodeType.Contains:
                    return QueryNode.Contains(Parse(reader), Parse(reader));
            default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}