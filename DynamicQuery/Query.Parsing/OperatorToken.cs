﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using DynamicQuery.Query.Syntax;

namespace DynamicQuery.Query.Parsing
{
    public static class OperatorToken
    {
        private static Dictionary<string, QueryNodeType> mappings;

        public static QueryNodeType MapName(string name)
        {
            if (mappings == null)
            {
                mappings = typeof (OperatorToken)
                    .GetFields(BindingFlags.Public | BindingFlags.Static)
                    .ToDictionary(
                        k => (string) k.GetValue(null),
                        v => (QueryNodeType) Enum.Parse(typeof (QueryNodeType), v.Name));
            }

            return mappings[name];
        }

        public static string Filter = "filter";
        public static string Not = "!";
        public static string Equal = "=";
        public static string NotEqual = "!=";
        public static string LessThan = "<";
        public static string LessThanOrEqual = "<=";
        public static string GreaterThan = ">";
        public static string GreaterThanOrEqual = ">=";
        public static string LogicalAnd = "and";
        public static string LogicalOr = "or";
        public static string BitwiseAnd = "&";
        public static string BitwiseOr = "|";
        public static string Between = "between";
        public static string StartsWith = "startswith";
        public static string EndsWith = "endswith";
        public static string Contains = "contains";
        public static string Member = "member";
        public static string Select = "select";
        public static string Sort = "sort";
        public static string Ascending = "asc";
        public static string Descending = "desc";
        public static string Pipe = "pipe";
        public static string Aggregate = "aggregate";
        public static string Sum = "sum";
        public static string Maximum = "max";
        public static string Minimum = "min";
        public static string Over = "over";
        public static string Repeat = "repeat";
        public static string Count = "count";
        public static string Limit = "limit";
    }
}