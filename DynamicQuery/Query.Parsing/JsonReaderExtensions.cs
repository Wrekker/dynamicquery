using Newtonsoft.Json;

namespace DynamicQuery.Query.Parsing
{
    public static class JsonReaderExtensions
    {
        public static bool Accept(this JsonReader reader, JsonToken token)
        {
            object _;
            return Accept(reader, token, out _);
        }

        public static T PeekValue<T>(this JsonReader reader)
        {
            return (T) reader.Value;
        }

        public static T ReadValue<T>(this JsonReader reader)
        {
            var ret = (T) reader.Value;
            reader.Read();
            return ret;
        }

        public static bool Accept(this JsonReader reader, JsonToken token, out object value)
        {
            if (reader.TokenType != token)
            {
                value = null;
                return false;
            }

            value = reader.Value;
            reader.Read();
            return true;
        }
        
        public static void Expect(this JsonReader reader, JsonToken token)
        {
            if (reader.TokenType != token)
            {
                throw new JsonReaderException("Expected " + token + ", found " + reader.TokenType);
            }
            reader.Read();
        }
    }
}