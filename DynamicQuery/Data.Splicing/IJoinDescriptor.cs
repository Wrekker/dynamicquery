using System;
using System.Collections.Generic;

namespace DynamicQuery.Data.Splicing
{
    public interface IJoinDescriptor
    {
        Type OuterType { get; }
        Type InnerType { get; }
        IEnumerable<JoinRelation> GetRelations();
    }
}