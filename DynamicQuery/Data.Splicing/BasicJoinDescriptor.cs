using System;
using System.Collections.Generic;

namespace DynamicQuery.Data.Splicing
{
    public class BasicJoinDescriptor : IJoinDescriptor
    {
        private readonly JoinRelation[] _relations;

        public Type OuterType { get; private set; }
        public Type InnerType { get; private set; }

        public BasicJoinDescriptor(Type outerType, Type innerType, params JoinRelation[] relations)
        {
            _relations = relations;
            OuterType = outerType;
            InnerType = innerType;
        }

        public IEnumerable<JoinRelation> GetRelations()
        {
            return _relations;
        }
    }
}