namespace DynamicQuery.Data.Splicing
{
    public class JoinRelation
    {
        public string OuterField { get; private set; }
        public string InnerField { get; private set; }

        public JoinRelation(string outerField, string innerField)
        {
            OuterField = outerField;
            InnerField = innerField;
        }
    }
}