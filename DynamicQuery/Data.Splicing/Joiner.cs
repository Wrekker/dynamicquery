﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using DynamicQuery.Data.Mapping;
using DynamicQuery.Data.Types;
using DynamicQuery.Types;

namespace DynamicQuery.Data.Splicing
{
    public class Joiner
    {
        private const string OuterPropertyName = "Outer";
        private const string InnerPropertyName = "Inner";

        private readonly IDynamicAssemblyBuilder _assemblyBuilder;

        public Joiner(IDynamicAssemblyBuilder assemblyBuilder)
        {
            _assemblyBuilder = assemblyBuilder;
        }

        public IQueryable Join(IQueryable outer, IQueryable inner, IJoinDescriptor joinDescriptor)
        {
            var groupJoinGenerator = MakeGroupJoinGenerator(joinDescriptor);
            var joined = inner.Provider.CreateQuery(groupJoinGenerator(outer, inner));

            var selectManyGenerator = MakeSelectManyGenerator(joinDescriptor, joined.ElementType);

            return joined.Provider.CreateQuery(selectManyGenerator(joined));
        }

        private Func<IQueryable, Expression> MakeSelectManyGenerator(IJoinDescriptor descriptor, Type inType)
        {
            var outerFields = descriptor.OuterType.GetProperties();
            var innerFields = descriptor.InnerType.GetProperties();
            var outerAliases = new List<string>();
            var innerAliases = new List<string>();
            
            var resultType = _assemblyBuilder.CreateType(t =>
            {
                foreach (var propertyInfo in outerFields)
                {
                    var fieldName = Identifier.DiscriminatedProperty(descriptor.OuterType, propertyInfo, OuterPropertyName);
                    t.Field(
                        propertyInfo.PropertyType,
                        fieldName,
                        f => f
                            .IsWritable()
                            .IncludeInEquality());

                    outerAliases.Add(fieldName);
                }

                foreach (var propertyInfo in innerFields)
                {
                    var fieldName = Identifier.DiscriminatedProperty(descriptor.InnerType, propertyInfo, InnerPropertyName);
                    t.Field(
                        propertyInfo.PropertyType.BoxToNullable(),
                        fieldName,
                        f => f
                            .IsWritable()
                            .IncludeInEquality());

                    innerAliases.Add(fieldName);
                }

                return t.WithDefaultConstructor();
            });
            
            var defaultMethod = MethodFactory.MakeDefaultIfEmpty(descriptor.InnerType);
            var collectionParameter = Expression.Parameter(inType);
            var collectionSelector = Expression.Lambda(
                Expression.Call(
                    defaultMethod,
                    Expression.Property(collectionParameter, InnerPropertyName)),
                collectionParameter);

            var outerParameter = Expression.Parameter(inType);
            var outerProjectBindings = outerFields
                .Zip(outerAliases, Tuple.Create)
                .Select(s => Expression.Bind(
                    resultType.GetProperty(s.Item2),
                    Expression.Property(
                        Expression.Property(outerParameter, OuterPropertyName),
                        s.Item1)));

            var innerParameter = Expression.Parameter(descriptor.InnerType);
            var innerProjectBindings = innerFields
                .Zip(innerAliases, Tuple.Create)
                .Select(s =>
                {
                    var bindToProperty = resultType.GetProperty(s.Item2);
                    var boxedPropertyType = s.Item1.PropertyType.BoxToNullable();

                    return Expression.Bind(
                        bindToProperty,
                        Expression.Condition(
                            Expression.NotEqual(innerParameter, Expression.Constant(null)),
                            Expression.Property(innerParameter, s.Item1).Convert(boxedPropertyType),
                            Expression.Constant(null, boxedPropertyType)));
                });

            var resultConstructor = resultType.GetConstructor(Type.EmptyTypes);
            var resultBindings = outerProjectBindings.Concat(innerProjectBindings);
            var resultSelector = Expression.Lambda(
                Expression.MemberInit(
                    Expression.New(resultConstructor),
                    resultBindings),
                outerParameter,
                innerParameter);

            var selectManyMethod = MethodFactory.MakeSelectMany(inType, descriptor.InnerType, resultType);

            return groups => Expression.Call(
                selectManyMethod,
                groups.Expression,
                collectionSelector,
                resultSelector);
        }

        private Func<IQueryable, IQueryable, Expression> MakeGroupJoinGenerator(IJoinDescriptor descriptor)
        {
            var outerFields = new List<PropertyInfo>();
            var innerFields = new List<PropertyInfo>();

            foreach (var r in descriptor.GetRelations())
            {
                outerFields.Add(descriptor.OuterType.GetProperty(r.OuterField));
                innerFields.Add(descriptor.InnerType.GetProperty(r.InnerField));
            }

            var outerResultType = descriptor.OuterType;
            var innerResultType = typeof(IEnumerable<>).MakeGenericType(descriptor.InnerType);
             
            var keyType = _assemblyBuilder.CreateType(t =>
            {
                foreach (var propertyInfo in outerFields)
                {
                    t.Field(
                        propertyInfo.PropertyType,
                        Identifier.Anonymous(),
                        f => f
                            .IsWritable()
                            .IncludeInEquality());
                }

                return t.WithDefaultConstructor();
            });
            var keyConstructor = keyType.GetConstructor(Type.EmptyTypes);
            var keyFields = keyType.GetProperties();

            var outerParameter = Expression.Parameter(descriptor.OuterType);
            var outerBindings = outerFields
                .Zip(keyFields, Tuple.Create)
                .Select(s => Expression.Bind(s.Item2, Expression.Property(outerParameter, s.Item1)));
            var outerSelector = Expression.Lambda(
                Expression.MemberInit(
                    Expression.New(keyConstructor),
                    outerBindings),
                outerParameter);

            var innerParameter = Expression.Parameter(descriptor.InnerType);
            var innerBindings = innerFields
                .Zip(keyFields, Tuple.Create)
                .Select(s => Expression.Bind(s.Item2, Expression.Property(innerParameter, s.Item1)));
            var innerSelector = Expression.Lambda(
                Expression.MemberInit(
                    Expression.New(keyConstructor),
                    innerBindings),
                innerParameter);

            var resultType = _assemblyBuilder.CreateType(t => t
                .WithDefaultConstructor()
                .Field(outerResultType, OuterPropertyName, f => f
                    .IsWritable()
                    .IncludeInEquality())
                .Field(innerResultType, InnerPropertyName, f => f
                    .IsWritable()
                    .IncludeInEquality()));
            var resultConstructor = resultType.GetConstructor(Type.EmptyTypes);
            var outerResultParameter = Expression.Parameter(outerResultType);
            var innerResultParameter = Expression.Parameter(innerResultType);
            var resultBindings = new[]
            {
                Expression.Bind(resultType.GetProperty(OuterPropertyName), outerResultParameter),
                Expression.Bind(resultType.GetProperty(InnerPropertyName), innerResultParameter)
            };

            var resultSelector = Expression.Lambda(
                Expression.MemberInit(
                    Expression.New(resultConstructor),
                    resultBindings),
                outerResultParameter,
                innerResultParameter);
            
            var groupJoinMethod = MethodFactory.MakeGroupJoin(
                descriptor.OuterType,
                descriptor.InnerType,
                keyType,
                resultType);

            return (outer, inner) =>
                Expression.Call(
                    groupJoinMethod,
                    new[]
                    {
                        outer.Expression,
                        inner.Expression,
                        outerSelector,
                        innerSelector,
                        resultSelector
                    });
        }
    }
}
