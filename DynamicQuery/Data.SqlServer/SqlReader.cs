using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DynamicQuery.Data.SqlServer
{
    public class SqlReader : IDisposable
    {
        private readonly SqlConnection _connection;
        private bool _opened;

        public SqlReader(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
        }
          
        public IEnumerable<T> Read<T>(string sql, Func<IDataReader, T> mapper, params SqlParameter[] parameters)
        {
            if (!_opened)
            {
                _connection.Open();
                _opened = true;
            }

            using (var command = _connection.CreateCommand())
            {
                command.CommandText = sql;
                command.Parameters.AddRange(parameters);
                using (var datareader = command.ExecuteReader())
                {
                    while (datareader.Read())
                    {
                        yield return mapper(datareader);
                    }
                }
            }
        }

        public int Execute(string sql, params SqlParameter[] parameters)
        {
            if (!_opened)
            {
                _connection.Open();
                _opened = true;
            }

            using (var command = _connection.CreateCommand())
            {
                command.CommandText = sql;
                command.Parameters.AddRange(parameters);
                return command.ExecuteNonQuery();
            }
        }

        public void Dispose()
        {
            if (_connection.State == ConnectionState.Open)
            {
                _connection.Close();
            }
            _connection.Dispose();
        }
    }
}