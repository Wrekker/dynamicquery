using System.Collections.Generic;

namespace DynamicQuery.Query.Syntax
{
    public class AggregateNode : IQueryNode
    {
        public QueryNodeType NodeType { get { return QueryNodeType.Aggregate; } }
        public IReadOnlyList<AggregatorNode> Aggregators { get; private set; }
        public OverNode Over { get; private set; }

        internal AggregateNode(OverNode over, IReadOnlyList<AggregatorNode> aggregators)
        {
            Over = over;
            Aggregators = aggregators;
        }
    }
}