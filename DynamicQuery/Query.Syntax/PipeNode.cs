using System.Collections.Generic;

namespace DynamicQuery.Query.Syntax
{
    public class PipeNode : IQueryNode
    {
        public QueryNodeType NodeType { get { return QueryNodeType.Pipe; } }

        public IReadOnlyList<IQueryNode> Pipings { get; private set; }

        internal PipeNode(IReadOnlyList<IQueryNode> pipings)
        {
            Pipings = pipings;
        }
    }
}