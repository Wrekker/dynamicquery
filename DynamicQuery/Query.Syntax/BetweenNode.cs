﻿namespace DynamicQuery.Query.Syntax
{
    public class BetweenNode : IQueryNode
    {
        public IQueryNode Value { get; private set; }
        public IQueryNode Lower { get; private set; }
        public IQueryNode Upper { get; private set; }
        
        public QueryNodeType NodeType { get { return QueryNodeType.Between; } }

        internal BetweenNode(IQueryNode value, IQueryNode lower, IQueryNode upper)
        {
            Value = value;
            Lower = lower;
            Upper = upper;
        }
    }
}