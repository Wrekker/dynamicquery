namespace DynamicQuery.Query.Syntax
{
    public class UnaryNode : IQueryNode
    {
        public QueryNodeType NodeType { get; private set; }

        public IQueryNode Operand { get; private set; }

        internal UnaryNode(QueryNodeType nodeType, IQueryNode operand)
        {
            NodeType = nodeType;
            Operand = operand;
        }
    }
}