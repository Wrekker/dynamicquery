﻿using System.Collections.Generic;
using System.Linq;

namespace DynamicQuery.Query.Syntax
{
    public static class QueryNode
    {
        public static IQueryNode Not(IQueryNode left, IQueryNode right)
        {
            return new BinaryNode(QueryNodeType.Not, left, right);
        }

        public static IQueryNode Equal(IQueryNode left, IQueryNode right)
        {
            return new BinaryNode(QueryNodeType.Equal, left, right);
        }

        public static IQueryNode NotEqual(IQueryNode left, IQueryNode right)
        {
            return new BinaryNode(QueryNodeType.NotEqual, left, right);
        }

        public static IQueryNode LessThan(IQueryNode left, IQueryNode right)
        {
            return new BinaryNode(QueryNodeType.LessThan, left, right);
        }

        public static IQueryNode LessThanOrEqual(IQueryNode left, IQueryNode right)
        {
            return new BinaryNode(QueryNodeType.LessThanOrEqual, left, right);
        }

        public static IQueryNode GreaterThan(IQueryNode left, IQueryNode right)
        {
            return new BinaryNode(QueryNodeType.GreaterThan, left, right);
        }

        public static IQueryNode GreaterThanOrEqual(IQueryNode left, IQueryNode right)
        {
            return new BinaryNode(QueryNodeType.GreaterThanOrEqual, left, right);
        }

        public static IQueryNode LogicalAnd(IQueryNode left, IQueryNode right)
        {
            return new BinaryNode(QueryNodeType.LogicalAnd, left, right);
        }

        public static IQueryNode LogicalOr(IQueryNode left, IQueryNode right)
        {
            return new BinaryNode(QueryNodeType.LogicalOr, left, right);
        }

        public static IQueryNode BitwiseAnd(IQueryNode left, IQueryNode right)
        {
            return new BinaryNode(QueryNodeType.BitwiseAnd, left, right);
        }

        public static IQueryNode BitwiseOr(IQueryNode left, IQueryNode right)
        {
            return new BinaryNode(QueryNodeType.BitwiseOr, left, right);
        }

        public static IQueryNode StartsWith(IQueryNode left, IQueryNode right)
        {
            return new BinaryNode(QueryNodeType.StartsWith, left, right);
        }

        public static IQueryNode EndsWith(IQueryNode left, IQueryNode right)
        {
            return new BinaryNode(QueryNodeType.EndsWith, left, right);
        }

        public static IQueryNode Contains(IQueryNode left, IQueryNode right)
        {
            return new BinaryNode(QueryNodeType.Contains, left, right);
        }

        public static IQueryNode Between(IQueryNode value, IQueryNode lower, IQueryNode upper)
        {
            return new BetweenNode(value, lower, upper);
        }

        internal static IQueryNode Not(IQueryNode operand)
        {
            return new UnaryNode(QueryNodeType.Not, operand);
        }

        public static IQueryNode Sum(IQueryNode member)
        {
            return new AggregatorNode(QueryNodeType.Sum, (MemberNode)member);
        }

        public static IQueryNode Average(IQueryNode member)
        {
            return new AggregatorNode(QueryNodeType.Average, (MemberNode)member);
        }

        public static IQueryNode Minimum(IQueryNode member)
        {
            return new AggregatorNode(QueryNodeType.Minimum, (MemberNode)member);
        }

        public static IQueryNode Maximum(IQueryNode member)
        {
            return new AggregatorNode(QueryNodeType.Maximum, (MemberNode)member);
        }

        public static IQueryNode Repeat(IQueryNode member, IQueryNode literal)
        {
            return new RepeatNode((MemberNode)member, (LiteralNode)literal);
        }
        
        public static IQueryNode Count(IQueryNode member)
        {
            return new AggregatorNode(QueryNodeType.Count, (MemberNode)member);
        }

        public static IQueryNode Aggregate(IQueryNode over, IEnumerable<IQueryNode> aggregators)
        {
            return new AggregateNode(
                (OverNode) over,
                aggregators
                    .Cast<AggregatorNode>()
                    .ToArray());
        }

        public static IQueryNode Over()
        {
            return Over(Enumerable.Empty<IQueryNode>());
        }

        public static IQueryNode Over(IEnumerable<IQueryNode> members)
        {
            return new OverNode(
                members
                    .Cast<MemberNode>()
                    .ToArray());
        }

        public static IQueryNode Pipe(IEnumerable<IQueryNode> nodes)
        {
            return new PipeNode(nodes.ToArray());
        }

        public static IQueryNode Select(IEnumerable<IQueryNode> nodes)
        {
            return new SelectNode(nodes.ToArray());
        }

        public static IQueryNode Ascending(IQueryNode member)
        {
            return new OrderingNode((MemberNode) member, false);
        }

        public static IQueryNode Descending(IQueryNode member)
        {
            return new OrderingNode((MemberNode)member, true);
        }

        public static IQueryNode Sort(IEnumerable<IQueryNode> orderings)
        {
            return Sort(orderings, null);
        }

        public static IQueryNode Sort(IEnumerable<IQueryNode> orderings, IQueryNode limit)
        {
            return new SortNode(
                orderings
                    .Cast<OrderingNode>()
                    .ToArray(),
                (LimitNode) limit);
        }

        public static IQueryNode Limit(IQueryNode skip, IQueryNode take)
        {
            return new LimitNode(
                (LiteralNode)skip,
                (LiteralNode)take);
        }

        public static IQueryNode Filter(IQueryNode filter)
        {
            return new FilterNode(filter);
        }

        public static IQueryNode Member(string name)
        {
            return new MemberNode(name);
        }

        public static IQueryNode Literal(object value)
        {
            return new LiteralNode(value);
        }
    }
}