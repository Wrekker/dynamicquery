namespace DynamicQuery.Query.Syntax
{
    public interface IQueryNode
    {
        QueryNodeType NodeType { get; }
    }
}