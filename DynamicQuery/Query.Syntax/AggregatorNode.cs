namespace DynamicQuery.Query.Syntax
{
    public class AggregatorNode : IQueryNode
    {
        public MemberNode Member { get; private set; }

        public QueryNodeType NodeType { get; private set; }

        internal AggregatorNode(QueryNodeType nodeType, MemberNode member)
        {
            NodeType = nodeType;
            Member = member;
        }
    }
}