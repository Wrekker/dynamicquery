namespace DynamicQuery.Query.Syntax
{
    public class BinaryNode : IQueryNode
    {
        public QueryNodeType NodeType { get; private set; }
        public IQueryNode Left { get; private set; }
        public IQueryNode Right { get; private set; }

        internal BinaryNode(QueryNodeType nodeType, IQueryNode left, IQueryNode right)
        {
            NodeType = nodeType;
            Left = left;
            Right = right;
        }
    }
}