using System.Collections.Generic;

namespace DynamicQuery.Query.Syntax
{
    public class SelectNode : IQueryNode
    {
        public QueryNodeType NodeType { get { return QueryNodeType.Select; } }

        public IReadOnlyList<IQueryNode> Fields { get; private set; }

        internal SelectNode(IReadOnlyList<IQueryNode> fields)
        {
            Fields = fields;
        }
    }
}