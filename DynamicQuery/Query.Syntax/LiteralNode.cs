namespace DynamicQuery.Query.Syntax
{
    public class LiteralNode : IQueryNode
    {
        public QueryNodeType NodeType { get { return QueryNodeType.Literal; } }

        public object Value { get; private set; }

        internal LiteralNode(object value)
        {
            Value = value;
        }
    }
}