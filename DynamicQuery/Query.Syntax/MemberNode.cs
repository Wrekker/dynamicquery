namespace DynamicQuery.Query.Syntax
{
    public class MemberNode : IQueryNode
    {
        public QueryNodeType NodeType { get { return QueryNodeType.Member; } }

        public string MemberName { get; private set; }

        internal MemberNode(string memberName)
        {
            MemberName = memberName;
        }
    }
}