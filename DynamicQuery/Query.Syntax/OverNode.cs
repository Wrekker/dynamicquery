using System.Collections.Generic;

namespace DynamicQuery.Query.Syntax
{
    public class OverNode : IQueryNode
    {
        public QueryNodeType NodeType { get { return QueryNodeType.Over; } }
        public IReadOnlyList<MemberNode> Members { get; private set; }

        internal OverNode(IReadOnlyList<MemberNode> members)
        {
            Members = members;
        }
    }
}