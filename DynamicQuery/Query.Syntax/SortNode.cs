using System.Collections.Generic;

namespace DynamicQuery.Query.Syntax
{
    public class SortNode : IQueryNode
    {
        public QueryNodeType NodeType { get { return QueryNodeType.Sort; } }
        public IReadOnlyList<OrderingNode> Orderings { get; private set; }
        public LimitNode Limit { get; private set; }

        internal SortNode(IReadOnlyList<OrderingNode> orderings, LimitNode limit)
        {
            Orderings = orderings;
            Limit = limit;
        }
    }
}