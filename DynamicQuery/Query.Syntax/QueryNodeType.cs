namespace DynamicQuery.Query.Syntax
{
    public enum QueryNodeType
    {
        None,

        Member,
        Literal,

        Not,
        Equal,
        NotEqual,
        LessThan,
        LessThanOrEqual,
        GreaterThan,
        GreaterThanOrEqual,
        LogicalAnd,
        LogicalOr,
        BitwiseAnd,
        BitwiseOr,
        StartsWith,
        EndsWith,
        Contains,

        Between,

        Filter,

        Aggregate,
        Sum,
        Average,
        Minimum,
        Maximum,
        Repeat,
        Count,
        Over,

        Select,

        Sort,
        Ascending,
        Descending,
        Limit,

        Pipe
    }
}