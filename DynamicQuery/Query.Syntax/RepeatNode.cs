namespace DynamicQuery.Query.Syntax
{
    public class RepeatNode : AggregatorNode
    {
        public LiteralNode Expression { get; private set; }

        internal RepeatNode(MemberNode member, LiteralNode expression) : base(QueryNodeType.Repeat, member)
        {
            Expression = expression;
        }
    }
}