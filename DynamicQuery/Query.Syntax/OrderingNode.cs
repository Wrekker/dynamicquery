namespace DynamicQuery.Query.Syntax
{
    public class OrderingNode : IQueryNode
    {
        public QueryNodeType NodeType
        {
            get
            {
                return IsDescending 
                    ? QueryNodeType.Descending 
                    : QueryNodeType.Ascending;
            }
        }

        public MemberNode Member { get; private set; }
        public bool IsDescending { get; private set; }

        internal OrderingNode(MemberNode member, bool isDescending)
        {
            Member = member;
            IsDescending = isDescending;
        }
    }
}