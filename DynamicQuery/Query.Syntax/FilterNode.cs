namespace DynamicQuery.Query.Syntax
{
    public class FilterNode : IQueryNode
    {
        public QueryNodeType NodeType { get { return QueryNodeType.Filter; } }
        public IQueryNode Predicate { get; private set; }

        internal FilterNode(IQueryNode predicate)
        {
            Predicate = predicate;
        }
    }
}