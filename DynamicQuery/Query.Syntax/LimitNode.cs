namespace DynamicQuery.Query.Syntax
{
    public class LimitNode : IQueryNode
    {
        public QueryNodeType NodeType { get { return QueryNodeType.Limit; } }
        public LiteralNode Skip { get; private set; }
        public LiteralNode Take { get; private set; }

        public LimitNode(LiteralNode skip, LiteralNode take)
        {
            Skip = skip;
            Take = take;
        }
    }
}