﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DynamicQuery.Data.Mapping
{
    public interface IFieldMappings
    {
        void Map(string from, string to);
        string Lookup(string origin);
        void Delete(string name);
    }

    public class FieldMappings : IFieldMappings
    {
        private IDictionary<string, string> _reverseMappings;
        private IDictionary<string, string> _forwardMappings;

        public FieldMappings(IEnumerable<string> fields)
        {
            _forwardMappings = fields.ToDictionary(k => k);
            _reverseMappings = new Dictionary<string, string>();
        }

        public void Map(string from, string to)
        {
            var current = _reverseMappings[from];

            _forwardMappings[current] = to;
            _reverseMappings[from] = to;
        }

        public void Delete(string name)
        {
            var current = _reverseMappings[name];

            _forwardMappings.Remove(current);
            _reverseMappings.Remove(name);
        }

        public string Lookup(string origin)
        {
            return _forwardMappings[origin];
        }
    }
}