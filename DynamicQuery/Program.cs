﻿using System;
using System.IO;
using System.Linq;
using DynamicQuery.Data.Types;
using DynamicQuery.Query.Generation;
using DynamicQuery.Query.Parsing;
using DynamicQuery.Query.Syntax;
using Newtonsoft.Json;

namespace DynamicQuery
{
    class Program
    {
        public class Data
        {
            public int Id { get; set; }
            public string Gender { get; set; }
            [JsonProperty("first_name")]
            public string FirstName { get; set; }
            [JsonProperty("last_name")]
            public string LastName { get; set; }
            public string Email { get; set; }
            [JsonProperty("ip_address")]
            public string IpAddress { get; set; } 
        }

        private static void Main(string[] args)
        {
            var query = new JsonQueryDeserializer().Deserialize(new JsonTextReader(new StreamReader("../../../query.js")));
            var data = JsonConvert.DeserializeObject<Data[]>(File.ReadAllText("../../../sample_data.json"));

            var piping = new PipingGenerator(
                new FilterGenerator(new LogicalGenerator(new TerminalGenerator())),
                new AggregationGenerator(new AggregatorGenerator(), new DynamicAssemblyBuilder("Assembly1")),
                new SelectGenerator(new TerminalGenerator(), new DynamicAssemblyBuilder("Assembly2")),
                new SortGenerator(new TerminalGenerator()));

            var pipe = piping.Generate(typeof (Data), (PipeNode)query);

            var result = pipe.Function(data.AsQueryable())
                .Cast<object>();

            foreach (var r in result)
            {
                Console.WriteLine(r);
            }
            
            Console.Read();
        }
    }
}
