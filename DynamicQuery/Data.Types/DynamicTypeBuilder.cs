﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace DynamicQuery.Data.Types
{
    internal class DynamicTypeBuilder : IDynamicTypeBuilder
    {
        private readonly IList<IPropertyBuilder> _properties = new List<IPropertyBuilder>();
        private readonly IList<Type> _interfaces = new List<Type>();
        private bool _hasDefaultConstructor = false;
        private bool _hasInitializationConstructor = false;

        public string TypeName { get; private set; }

        public DynamicTypeBuilder(string typeName)
        {
            TypeName = typeName;
        }

        public IDynamicTypeBuilder Implements<T>()
        {
            _interfaces.Add(typeof(T));

            return this;
        }
        
        public IDynamicTypeBuilder Field(Type fieldType, string name, Func<IPropertyBuilder, IPropertyBuilder> builder = null)
        {
            var dynamicFieldBuilder = new DynamicPropertyBuilder(fieldType, name);
            var configuredBuilder = builder != null
                ? builder(dynamicFieldBuilder)
                : dynamicFieldBuilder;
            
            _properties.Add(configuredBuilder);
            
            return this;
        }

        public IDynamicTypeBuilder WithDefaultConstructor()
        {
            _hasDefaultConstructor = true;

            return this;
        }

        public IDynamicTypeBuilder WithInitializationConstructor()
        {
            _hasInitializationConstructor = true;

            return this;
        }

        public Type Build(ModuleBuilder module)
        {
            var typeBuilder = module.DefineType(TypeName);
            
            var properties = BuildProperties(typeBuilder, _properties);
            var equals = BuildEquals(typeBuilder, properties);
            var hashCode = BuildHashcode(typeBuilder, properties);
            var toString = BuildToString(typeBuilder, properties);

            var defaultConstructor = _hasDefaultConstructor
                ? BuildDefaultConstructor(typeBuilder)
                : null;

            var initializationConstructor = _hasInitializationConstructor
                ? BuildInitializationConstructor(typeBuilder, properties)
                : null;

            AttachInterfaces(typeBuilder);
            AttachAttributes(typeBuilder, properties, equals, hashCode, toString);

            return typeBuilder.CreateType();
        }

        private ConstructorBuilder BuildDefaultConstructor(TypeBuilder builder)
        {
            var defaultConstructor = builder.DefineConstructor(
                MethodAttributes.Public,
                CallingConventions.Standard,
                Type.EmptyTypes);

            var defaultCtorIl = defaultConstructor.GetILGenerator();
            defaultCtorIl.Emit(OpCodes.Ret);

            return defaultConstructor;
        }

        private ConstructorBuilder BuildInitializationConstructor(TypeBuilder builder, IReadOnlyList<DynamicProperty> properties)
        {
            // Initialization constructor
            var parameterTypes = properties.Select(s => s.FieldBuilder.FieldType).ToArray();

            var initConstructor = builder.DefineConstructor(
                MethodAttributes.Public,
                CallingConventions.Standard,
                parameterTypes);

            var initCtorIl = initConstructor.GetILGenerator();
            
            for (var i = 0; i < properties.Count; i++)
            {
                initCtorIl.Emit(OpCodes.Ldarg_0);
                initCtorIl.Emit(OpCodes.Ldarg, i + 1);
                initCtorIl.Emit(OpCodes.Stfld, properties[i].FieldBuilder);
            }

            initCtorIl.Emit(OpCodes.Ret);

            return initConstructor;
        }

        private IReadOnlyList<DynamicProperty> BuildProperties(TypeBuilder builder, IList<IPropertyBuilder> properties)
        {
            return properties
                .Select(s => s.Build(builder))
                .ToArray();
        }

        private MethodBuilder BuildEquals(TypeBuilder builder, IReadOnlyList<DynamicProperty> properties)
        {
            var equals = builder.DefineMethod(
                "Equals",
                MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.SpecialName | MethodAttributes.HideBySig,
                typeof(bool),
                new [] { typeof(object) });
            
            var il = equals.GetILGenerator();
            
            var inequalLabel = il.DefineLabel();
            
            // arg1 is not anonType => inequal
            il.Emit(OpCodes.Ldarg_1);
            il.Emit(OpCodes.Isinst, builder);
            il.Emit(OpCodes.Brfalse, inequalLabel);

            foreach (var property in properties.Where(w => w.IncludedInEquality))
            {
                var field = property.FieldBuilder;

                var comparerType = typeof (EqualityComparer<>).MakeGenericType(field.FieldType);
                var defaultComparer = comparerType.GetProperty("Default");
                var equalsMethod = comparerType.GetMethod("Equals", new[] { field.FieldType, field.FieldType });

                il.Emit(OpCodes.Call, defaultComparer.GetMethod);

                il.Emit(OpCodes.Ldarg_0);
                il.Emit(OpCodes.Ldfld, field);

                il.Emit(OpCodes.Ldarg_1);
                il.Emit(OpCodes.Ldfld, field);

                il.Emit(OpCodes.Callvirt, equalsMethod);
                il.Emit(OpCodes.Brfalse, inequalLabel);
            }

            il.Emit(OpCodes.Ldc_I4_1);
            il.Emit(OpCodes.Ret);

            il.MarkLabel(inequalLabel);
            il.Emit(OpCodes.Ldc_I4_0);
            il.Emit(OpCodes.Ret);

            return equals;
        }

        private MethodBuilder BuildHashcode(TypeBuilder builder, IReadOnlyList<DynamicProperty> properties)
        {
            var getHashCode = builder.DefineMethod(
                "GetHashCode",
                MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.SpecialName | MethodAttributes.HideBySig,
                typeof(int),
                Type.EmptyTypes);
            
            var il = getHashCode.GetILGenerator();
            
            il.Emit(OpCodes.Ldc_I4_0);
            foreach (var property in properties.Where(w => w.IncludedInEquality))
            {
                var field = property.FieldBuilder;

                il.Emit(OpCodes.Ldc_I4, 397);
                il.Emit(OpCodes.Mul);
                
                var comparerType = typeof(EqualityComparer<>).MakeGenericType(field.FieldType);
                var defaultComparerProperty = comparerType.GetProperty("Default");
                var hashcodeMethod = comparerType.GetMethod("GetHashCode", new [] { field.FieldType });
               
                il.Emit(OpCodes.Call, defaultComparerProperty.GetMethod); 
                il.Emit(OpCodes.Ldarg_0);
                il.Emit(OpCodes.Ldfld, field);
                il.Emit(OpCodes.Callvirt, hashcodeMethod);
                il.Emit(OpCodes.Add);
            }

            il.Emit(OpCodes.Ret);

            return getHashCode;
        }

        private MethodBuilder BuildToString(TypeBuilder builder, IReadOnlyList<DynamicProperty> properties)
        {
            var toString = builder.DefineMethod(
                "ToString",
                MethodAttributes.Public | MethodAttributes.Virtual | MethodAttributes.SpecialName | MethodAttributes.HideBySig,
                typeof(string),
                Type.EmptyTypes);
            

            var stringFormat = typeof(string).GetMethod(
                "Format", 
                new[] { typeof(string), typeof(object[]) });


            var pairs = properties.Select((s, i) => string.Format("{0} = {{{1}}}", s.PropertyBuilder.Name, i));
            var format = "{{ " + string.Join(", ", pairs) + " }}";

            var il = toString.GetILGenerator();

            il.Emit(OpCodes.Ldstr, format);

            il.Emit(OpCodes.Ldc_I4, properties.Count);
            il.Emit(OpCodes.Newarr, typeof(object));

            for (int index = 0; index < properties.Count(); index++)
            {
                var field = properties[index].FieldBuilder;

                il.Emit(OpCodes.Dup);

                il.Emit(OpCodes.Ldc_I4_S, index);

                il.Emit(OpCodes.Ldarg_0);
                il.Emit(OpCodes.Ldfld, field);
                il.Emit(OpCodes.Box, field.FieldType);

                il.Emit(OpCodes.Stelem, typeof(object));
            }

            il.Emit(OpCodes.Call, stringFormat);
            
            il.Emit(OpCodes.Ret);

            return toString;
        }

        private void AttachInterfaces(TypeBuilder builder)
        {
            foreach (var ifc in _interfaces)
            {
                builder.AddInterfaceImplementation(ifc);
            }
        }

        private void AttachAttributes(TypeBuilder typeBuilder, IReadOnlyList<DynamicProperty> properties, MethodBuilder equals, MethodBuilder getHashCode, MethodBuilder toString)
        {
            // Class
            {
                var constructor = typeof(DebuggerDisplayAttribute).GetConstructor(new[] { typeof(string) });
                var pairs = properties.Select((s, i) => string.Format("{0} = {{{0}}}", s.PropertyBuilder.Name));
                var format = "\\{ " + string.Join(", ", pairs) + " \\}";
                
                var cb = new CustomAttributeBuilder(constructor, new object[] { format });

                typeBuilder.SetCustomAttribute(cb);
            }
            // Methods
            {
                var constructor = typeof(DebuggerHiddenAttribute).GetConstructor(Type.EmptyTypes);

                equals.SetCustomAttribute(new CustomAttributeBuilder(constructor, new object[0]));
                toString.SetCustomAttribute(new CustomAttributeBuilder(constructor, new object[0]));
                getHashCode.SetCustomAttribute(new CustomAttributeBuilder(constructor, new object[0]));
            }
            // Fields
            {
                var constructor = typeof(DebuggerBrowsableAttribute).GetConstructor(new[] { typeof(DebuggerBrowsableState) });
                var never = new object[] { DebuggerBrowsableState.Never };

                foreach (var property in properties)
                {
                    property.FieldBuilder.SetCustomAttribute(new CustomAttributeBuilder(constructor, never));
                }
            }
        }
        
    }
}
