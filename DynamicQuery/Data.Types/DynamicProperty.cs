using System.Reflection.Emit;

namespace DynamicQuery.Data.Types
{
    public class DynamicProperty
    {
        public FieldBuilder FieldBuilder { get; set; }
        public PropertyBuilder PropertyBuilder { get; set; }

        public bool IncludedInEquality { get; set; }
        public bool IsWriteable { get; set; }
    }
}