using System;
using System.Reflection;
using System.Reflection.Emit;
using DynamicQuery.Types;

namespace DynamicQuery.Data.Types
{
    internal class DynamicAssemblyBuilder : IDynamicAssemblyBuilder
    {
        private int _counter;
        private readonly string _assemblyName;
        private readonly AssemblyBuilder _assemblyBuilder;
        private ModuleBuilder _moduleBuilder;

        public Assembly GeneratedAssembly { get { return _assemblyBuilder; } }

        public DynamicAssemblyBuilder(string name)
        {
            var assemblyName = string.Format("Dynamic.{0}.Assembly", name);
            var moduleName = string.Format("Dynamic.{0}.Assembly.Module", name);

            _assemblyName = name;
            _assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(new AssemblyName(assemblyName), AssemblyBuilderAccess.RunAndCollect);
            _moduleBuilder = _assemblyBuilder.DefineDynamicModule(moduleName);
        }

        public Type CreateType(Func<IDynamicTypeBuilder, IDynamicTypeBuilder> builder)
        {
            return CreateType(Identifier.Anonymous(), builder);
        }

        public Type CreateType(string typeName, Func<IDynamicTypeBuilder, IDynamicTypeBuilder> builder)
        {
            var fullName = string.Format("Dynamic.{0}.Assembly.Module.{1}", _assemblyName, typeName);
            var typeBuilder = builder(new DynamicTypeBuilder(fullName));

            return typeBuilder.Build(_moduleBuilder);
        }
    }
}