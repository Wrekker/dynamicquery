using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;

namespace DynamicQuery.Data.Types
{
    internal class DynamicPropertyBuilder : IPropertyBuilder
    {
        private bool _isWritable = false;
        private bool _includeInEquality = false;
        private readonly List<Tuple<Type, object[]>> _attributes = new List<Tuple<Type, object[]>>();

        public Type FieldType { get; private set; }
        public string Name { get; private set; }
        
        public bool IsInEquality { get; private set; }

        public DynamicPropertyBuilder(Type fieldType, string name)
        {
            FieldType = fieldType;
            Name = name;
        }

        IPropertyBuilder IPropertyBuilder.IsWritable()
        {
            _isWritable = true;
            return this;
        }

        IPropertyBuilder IPropertyBuilder.IncludeInEquality()
        {
            _includeInEquality = true;
            return this;
        }

        IPropertyBuilder IPropertyBuilder.Attribute(Type attributeType, params object[] parameters)
        {
            _attributes.Add(Tuple.Create(attributeType, parameters));
            return this;
        }

        DynamicProperty IPropertyBuilder.Build(TypeBuilder typeBuilder)
        {
            // Actual property
            var propertyBuilder = typeBuilder.DefineProperty(
                this.Name,
                PropertyAttributes.HasDefault,
                this.FieldType,
                null);
        
            // Backing field and getter
            var fieldBuilder = typeBuilder.DefineField(
                "_" + this.Name,
                this.FieldType,
                FieldAttributes.Private);

            var getAccessor = typeBuilder.DefineMethod(
                "get_" + this.Name,
                MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig,
                this.FieldType,
                Type.EmptyTypes);

            var getGenerator = getAccessor.GetILGenerator();
            getGenerator.Emit(OpCodes.Ldarg_0);
            getGenerator.Emit(OpCodes.Ldfld, fieldBuilder);
            getGenerator.Emit(OpCodes.Ret);

            propertyBuilder.SetGetMethod(getAccessor);

            // Setter
            if (_isWritable)
            {
                var setAccessor = typeBuilder.DefineMethod(
                    "set_" + this.Name,
                    MethodAttributes.Public | MethodAttributes.SpecialName | MethodAttributes.HideBySig,
                    typeof (void),
                    new[] {this.FieldType});

                var setGenerator = setAccessor.GetILGenerator();
                setGenerator.Emit(OpCodes.Ldarg_0);
                setGenerator.Emit(OpCodes.Ldarg_1);
                setGenerator.Emit(OpCodes.Stfld, fieldBuilder);
                setGenerator.Emit(OpCodes.Ret);

                propertyBuilder.SetSetMethod(setAccessor);
            }

            // Attributes
            foreach (var attribute in _attributes)
            {
                var constructor = attribute.Item1
                    .GetConstructors()
                    .FirstOrDefault(w => w.GetParameters().Length == attribute.Item2.Length);

                if (constructor == null)
                {
                    throw new NotSupportedException("No suitable attribute contructor found.");
                }

                fieldBuilder.SetCustomAttribute(new CustomAttributeBuilder(constructor, attribute.Item2));
            }

            return new DynamicProperty
            {
                FieldBuilder = fieldBuilder,
                PropertyBuilder = propertyBuilder,
                IncludedInEquality = _includeInEquality,
                IsWriteable = _isWritable
            };
        }
    }
}