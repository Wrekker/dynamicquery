using System;
using System.Reflection.Emit;

namespace DynamicQuery.Data.Types
{
    public interface IDynamicTypeBuilder
    {
        string TypeName { get; }

        IDynamicTypeBuilder Implements<T>();
        IDynamicTypeBuilder Field(Type fieldType, string name, Func<IPropertyBuilder, IPropertyBuilder> builder = null);
        IDynamicTypeBuilder WithDefaultConstructor();
        IDynamicTypeBuilder WithInitializationConstructor();

        Type Build(ModuleBuilder module);
    }
}