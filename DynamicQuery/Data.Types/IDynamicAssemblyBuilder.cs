using System;
using System.Reflection;

namespace DynamicQuery.Data.Types
{
    public interface IDynamicAssemblyBuilder
    {
        Assembly GeneratedAssembly { get; }
        Type CreateType(string typeName, Func<IDynamicTypeBuilder, IDynamicTypeBuilder> builder);
        Type CreateType(Func<IDynamicTypeBuilder, IDynamicTypeBuilder> builder);
    }
}