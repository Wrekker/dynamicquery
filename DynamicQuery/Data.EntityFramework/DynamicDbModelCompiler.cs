﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using DynamicQuery.Data.Types;

namespace DynamicQuery.Data.EntityFramework
{
    internal class DynamicDbModelCompiler
    {
        private readonly IDynamicAssemblyBuilder _assemblyBuilder;
        private readonly IEntityMetadataProvider _metadataProvider;

        public DynamicDbModelCompiler(IDynamicAssemblyBuilder assemblyBuilder, IEntityMetadataProvider metadataProvider)
        {
            _assemblyBuilder = assemblyBuilder;
            _metadataProvider = metadataProvider;
        }

        public DynamicDbModel CompileModel(DbConnection connection)
        {
            var mappings = new Dictionary<string, Type>();
            var modelBuilder = new DbModelBuilder();
            var generator = new EntityTypeConfigurationGenerator(_assemblyBuilder, _metadataProvider);
            var types = generator.GenerateEntityTypes();
            
            foreach (var type in types)
            {
                mappings.Add(type.Name, type);
                modelBuilder.RegisterEntityType(type);
            }

            var model = modelBuilder.Build(connection);
            var compiledModel = model.Compile();

            return new DynamicDbModel(compiledModel, mappings);
        }
    }
}