﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using DynamicQuery.Data.SqlServer;
using DynamicQuery.Data.Types;

namespace DynamicQuery.Data.EntityFramework
{
    public class DefaultDbContextFactory : IDbContextFactory<DbContext>
    {
        private readonly string _connectionString;
        private readonly IDynamicAssemblyBuilder _assemblyBuilder;
        private readonly IEntityMetadataProvider _metadataProvider;
        private readonly DynamicDbModelCompiler _dynamicDbModelCompiler;
        private readonly IDbConnectionFactory _connectionFactory;

        private DateTime _actualVersion = DateTime.UtcNow;
        private DateTime _compiledVersion = DateTime.MinValue;
        private DynamicDbModel _dynamicModel;

        public DateTime Version { get { return _compiledVersion; } }
        public IDictionary<string, Type> CustomFields { get { return _dynamicModel.Mappings; } }

        public DefaultDbContextFactory(string connectionString, IDynamicAssemblyBuilder assemblyBuilder, IEntityMetadataProvider metadataProvider)
        {
            _connectionString = connectionString;
            _assemblyBuilder = assemblyBuilder;
            _metadataProvider = metadataProvider;
            _connectionFactory = new SqlConnectionFactory();
            _dynamicDbModelCompiler = new DynamicDbModelCompiler(_assemblyBuilder, _metadataProvider);
        }

        public void Rebuild()
        {
            _actualVersion = DateTime.UtcNow;
            CustomFields.Clear();
        }
        
        public DbContext Create()
        {
            var connection = _connectionFactory.CreateConnection(_connectionString);

            if (_compiledVersion < _actualVersion)
            {
                _compiledVersion = _actualVersion;
                _dynamicModel = _dynamicDbModelCompiler.CompileModel(connection);
            }

            return new DbContext(connection, _dynamicModel.CompiledModel, true);
        }
    }
}