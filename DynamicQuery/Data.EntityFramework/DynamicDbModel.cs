using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;

namespace DynamicQuery.Data.EntityFramework
{
    internal class DynamicDbModel
    {
        public DbCompiledModel CompiledModel { get; private set; }
        public IDictionary<string, Type> Mappings { get; private set; }

        public DynamicDbModel(DbCompiledModel compiledModel, Dictionary<string, Type> mappings)
        {
            CompiledModel = compiledModel;
            Mappings = mappings;
        }
    }
}