﻿using System.Collections.Generic;
using System.Linq;

namespace DynamicQuery.Data.EntityFramework
{
    public static class QueryableExtensions
    {
        public static IEnumerable<object> Compile(this IQueryable queryable)
        {
            foreach (var q in queryable)
            {
                yield return q;
            }
        }
    }
}