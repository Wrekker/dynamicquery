﻿using System;
using System.Collections.Generic;
using System.Reflection;
using DynamicQuery.Data.Types;

namespace DynamicQuery.Data.EntityFramework
{
    public class EntityTypeConfigurationGenerator
    {
        private readonly IDynamicAssemblyBuilder _assemblyBuilder;
        private readonly IEntityMetadataProvider _provider;

        public EntityTypeConfigurationGenerator(IDynamicAssemblyBuilder assemblyBuilder, IEntityMetadataProvider provider)
        {
            _assemblyBuilder = assemblyBuilder;
            _provider = provider;
        }

        public IEnumerable<Type> GenerateEntityTypes()
        {
            var metadata = _provider.GetMetadata();

            foreach (var md in metadata)
            {
                yield return _assemblyBuilder.CreateType(md.EntityName, b =>
                {
                    b.WithDefaultConstructor();

                    foreach (var field in md.Fields)
                    {
                        b.Field(field.Type, field.Name, f => f
                            .IsWritable()
                            .IncludeInEquality());
                    }

                    return b;
                });
            }
        }
    }
} 