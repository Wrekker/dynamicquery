using System.Collections.Generic;

namespace DynamicQuery.Data.EntityFramework
{
    public class EntityMetadata
    {
        public string EntityName { get; set; }
        public IReadOnlyList<EntityField> Fields { get; set; }
    }
}