﻿using System.Collections.Generic;

namespace DynamicQuery.Data.EntityFramework
{
    public interface IEntityMetadataProvider
    {
        IReadOnlyList<EntityMetadata> GetMetadata();
    }
}