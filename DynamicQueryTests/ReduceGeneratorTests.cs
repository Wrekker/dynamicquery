﻿using System.Linq;
using DynamicQuery.Query.Syntax;
using DynamicQuery.Types;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DynamicQueryTests
{
    [TestClass]
    public class ReduceGeneratorTests
    {
        private PrimitiveDto[] dtos =
        {
            new PrimitiveDto { StringProperty = "a", IntProperty = 5, DoubleProperty = 1.5 },
            new PrimitiveDto { StringProperty = "a", IntProperty = 4, DoubleProperty = 1.5 },
            new PrimitiveDto { StringProperty = "a", IntProperty = 3, DoubleProperty = 2.8 },
            new PrimitiveDto { StringProperty = "a", IntProperty = 2, DoubleProperty = 2.8 },
            new PrimitiveDto { StringProperty = "a", IntProperty = 1, DoubleProperty = 2.8 }
        };
        
        [TestMethod]
        public void Reduce_To_Correct_Number_Of_Groups()
        {
            var reduce = Statics.AggregationGenerator.Generate(
                typeof (PrimitiveDto),
                (AggregateNode) QueryNode.Aggregate(
                    QueryNode.Over(new[] {QueryNode.Member("DoubleProperty")}),
                    new[]
                    {
                        QueryNode.Sum(QueryNode.Member("IntProperty"))
                    }));

            var result = reduce.Function(dtos.AsQueryable())
                .Cast<object>();
            
            Assert.AreEqual(
                2,
                result.Count());
        }

        [TestMethod]
        public void Reduce_To_Average()
        {
            var reduce = Statics.AggregationGenerator.Generate(
                typeof (PrimitiveDto),
                (AggregateNode) QueryNode.Aggregate(
                    QueryNode.Over(),
                    new[]
                    {
                        QueryNode.Average(QueryNode.Member("IntProperty"))
                    }));

            var result = reduce.Function(dtos.AsQueryable())
                .Cast<object>()
                .First();

            Assert.AreEqual(
                (int)((1 + 2 + 3 + 4 + 5) / 5d),
                result.GetDynamically<int>("IntProperty"));
        }

        [TestMethod]
        public void Reduce_To_Sum()
        {
            var reduce = Statics.AggregationGenerator.Generate(
                typeof (PrimitiveDto),
                (AggregateNode) QueryNode.Aggregate(
                    QueryNode.Over(),
                    new[]
                    {
                        QueryNode.Sum(QueryNode.Member("IntProperty")),
                    }));

            var result = reduce.Function(dtos.AsQueryable())
                .Cast<object>()
                .First();

            Assert.AreEqual(
                (1 + 2 + 3 + 4 + 5),
                result.GetDynamically<int>("IntProperty"));
        }

        [TestMethod]
        public void Reduce_To_Min()
        {
            var reduce = Statics.AggregationGenerator.Generate(
                typeof (PrimitiveDto),
                (AggregateNode) QueryNode.Aggregate(
                    QueryNode.Over(),
                    new[]
                    {
                        QueryNode.Minimum(QueryNode.Member("IntProperty")),
                    }));

            var result = reduce.Function(dtos.AsQueryable())
                .Cast<object>()
                .First();

            Assert.AreEqual(
                1,
                result.GetDynamically<int>("IntProperty"));
        }

        [TestMethod]
        public void Reduce_To_Max()
        {
            var reduce = Statics.AggregationGenerator.Generate(
                typeof (PrimitiveDto),
                (AggregateNode) QueryNode.Aggregate(
                    QueryNode.Over(),
                    new[]
                    {
                        QueryNode.Maximum(QueryNode.Member("IntProperty")),
                    }));

            var result = reduce.Function(dtos.AsQueryable())
                .Cast<object>()
                .First();

            Assert.AreEqual(
                5,
                result.GetDynamically<int>("IntProperty"));
        }

        [TestMethod]
        public void Reduce_With_Repeating_String()
        {
            var reduce = Statics.AggregationGenerator.Generate(
                typeof (PrimitiveDto),
                (AggregateNode) QueryNode.Aggregate(
                    QueryNode.Over(new[] { QueryNode.Member("DoubleProperty") }),
                    new[]
                    {
                        QueryNode.Sum(QueryNode.Member("IntProperty")),
                        QueryNode.Repeat(QueryNode.Member("StringProperty"), QueryNode.Literal("Literal")),
                    }));

            var result = reduce.Function(dtos.AsQueryable())
                .Cast<object>()
                .ToArray();
            
            Assert.IsTrue(
                Enumerable.Repeat("Literal", 2)
                    .SequenceEqual(result.Select(s => s.GetDynamically<string>("StringProperty"))));
        }
    }
}