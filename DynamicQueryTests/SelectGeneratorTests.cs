﻿using System.Linq;
using DynamicQuery.Query.Syntax;
using DynamicQuery.Types;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DynamicQueryTests
{
    [TestClass]
    public class SelectGeneratorTests
    {
        private PrimitiveDto[] dtos =
        {
            new PrimitiveDto { StringProperty = "a", IntProperty = 5, DoubleProperty = 1.5 },
            new PrimitiveDto { StringProperty = "b", IntProperty = 4, DoubleProperty = 1.5 },
            new PrimitiveDto { StringProperty = "c", IntProperty = 3, DoubleProperty = 2.8 },
            new PrimitiveDto { StringProperty = "d", IntProperty = 2, DoubleProperty = 2.8 },
            new PrimitiveDto { StringProperty = "e", IntProperty = 1, DoubleProperty = 2.8 }
        };
        
        [TestMethod]
        public void Select_String_Property_Only()
        {
            var reduce = Statics.SelectGenerator.Generate(
                typeof (PrimitiveDto),
                (SelectNode) QueryNode.Select(new[]
                {
                    QueryNode.Member("StringProperty")
                }));

            var result = reduce.Function(dtos.AsQueryable())
                .Cast<object>();

            var item = result.First();

            Assert.IsNull(item.GetType().GetProperty("IntProperty"));

            Assert.IsNull(item.GetType().GetProperty("DoubleProperty"));

            Assert.IsNotNull(item.GetType().GetProperty("StringProperty"));

            Assert.IsTrue(result
                .Select(s => s.GetDynamically<string>("StringProperty"))
                .SequenceEqual(dtos.Select(s => s.StringProperty)));
        }

        [TestMethod]
        public void Select_String_And_Double_Property_Only()
        {
            var reduce = Statics.SelectGenerator.Generate(
                typeof(PrimitiveDto),
                (SelectNode)QueryNode.Select(new[]
                {
                    QueryNode.Member("StringProperty"),
                    QueryNode.Member("DoubleProperty")
                }));

            var result = reduce.Function(dtos.AsQueryable())
                .Cast<object>();

            var item = result.First();

            Assert.IsNull(item.GetType().GetProperty("IntProperty"));

            Assert.IsNotNull(item.GetType().GetProperty("DoubleProperty"));

            Assert.IsNotNull(item.GetType().GetProperty("StringProperty"));

            Assert.IsTrue(result
                .Select(s => s.GetDynamically<double>("DoubleProperty"))
                .SequenceEqual(dtos.Select(s => s.DoubleProperty)));

            Assert.IsTrue(result
                .Select(s => s.GetDynamically<string>("StringProperty"))
                .SequenceEqual(dtos.Select(s => s.StringProperty)));
        }
    }
}