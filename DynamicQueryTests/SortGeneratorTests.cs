﻿using System.Linq;
using DynamicQuery.Query.Syntax;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DynamicQueryTests
{
    [TestClass]
    public class SortGeneratorTests
    {
        private PrimitiveDto[] dtos =
        {
            new PrimitiveDto {StringProperty = "abc", IntProperty = 1, DoubleProperty = 2 },
            new PrimitiveDto {StringProperty = "bcd", IntProperty = 2, DoubleProperty = 2 },
            new PrimitiveDto {StringProperty = "cde", IntProperty = 3, DoubleProperty = 1 },
            new PrimitiveDto {StringProperty = "def", IntProperty = 4, DoubleProperty = 1 }
        };


        [TestMethod]
        public void Ascending_Sort_Of_Integer_Member()
        {
            var node = QueryNode.Sort(new []
            {
                QueryNode.Ascending(QueryNode.Member("IntProperty")), 
            });

            var func = Statics.SortGenerator.Generate(typeof(PrimitiveDto), (SortNode) node);

            var sorted = func.Function(dtos.AsQueryable());

            Assert.IsTrue(
                sorted
                    .OfType<PrimitiveDto>()
                    .Select(s => s.IntProperty)
                    .SequenceEqual(new[] { 1, 2, 3, 4 }));
        }

        [TestMethod]
        public void Descending_Sort_Of_Integer_Member()
        {
            var node = QueryNode.Sort(new []
            {
                QueryNode.Descending(QueryNode.Member("IntProperty")),
            });

            var func = Statics.SortGenerator.Generate(typeof (PrimitiveDto), (SortNode) node);

            var sorted = func.Function(dtos.AsQueryable());

            Assert.IsTrue(
                sorted
                    .OfType<PrimitiveDto>()
                    .Select(s => s.IntProperty)
                    .SequenceEqual(new[] { 4, 3, 2, 1 }));
        }

        [TestMethod]
        public void Ascending_Sort_Of_String_Member()
        {
            var node = QueryNode.Sort(new []
            {
                QueryNode.Ascending(QueryNode.Member("StringProperty")),
            });

            var func = Statics.SortGenerator.Generate(typeof(PrimitiveDto), (SortNode) node);

            var sorted = func.Function(dtos.AsQueryable());
            
            Assert.IsTrue(
                sorted
                    .OfType<PrimitiveDto>()
                    .Select(s => s.StringProperty)
                    .SequenceEqual(new[] { "abc", "bcd", "cde", "def" }));
        }

        [TestMethod]
        public void Descending_Sort_Of_String_Member()
        {
            var node = QueryNode.Sort(new []
            {
                QueryNode.Descending(QueryNode.Member("StringProperty")),
            });

            var func = Statics.SortGenerator.Generate(typeof(PrimitiveDto), (SortNode) node);

            var sorted = func.Function(dtos.AsQueryable());

            Assert.IsTrue(
                sorted
                    .OfType<PrimitiveDto>()
                    .Select(s => s.StringProperty)
                    .SequenceEqual(new[] { "def", "cde", "bcd", "abc" }));
        }

        [TestMethod]
        public void Descending_Sort_Of_Double_Then_Int_Members()
        {
            var node = QueryNode.Sort(new []
            {
                QueryNode.Descending(QueryNode.Member("DoubleProperty")),
                QueryNode.Descending(QueryNode.Member("IntProperty")),
            });

            var func = Statics.SortGenerator.Generate(typeof(PrimitiveDto), (SortNode) node);

            var sorted = func.Function(dtos.AsQueryable());
            
            Assert.IsTrue(
                sorted
                    .OfType<PrimitiveDto>()
                    .Select(s => s.IntProperty)
                    .SequenceEqual(new[] {2, 1, 4, 3}));
        }

        [TestMethod]
        public void Limiting_Sort_First_Three_By_Int()
        {
            var node = QueryNode.Sort(
                new[] { QueryNode.Ascending(QueryNode.Member("IntProperty")) },
                QueryNode.Limit(QueryNode.Literal(0), QueryNode.Literal(3)));

            var func = Statics.SortGenerator.Generate(typeof(PrimitiveDto), (SortNode)node);

            var sorted = func.Function(dtos.AsQueryable());

            var expected = new[] {1, 2, 3};
            var actual = sorted
                .OfType<PrimitiveDto>()
                .Select(s => s.IntProperty);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }

        [TestMethod]
        public void Limiting_Sort_Last_Three_By_Int()
        {
            var node = QueryNode.Sort(
                new[] { QueryNode.Ascending(QueryNode.Member("IntProperty")) },
                QueryNode.Limit(QueryNode.Literal(1), QueryNode.Literal(3)));

            var func = Statics.SortGenerator.Generate(typeof(PrimitiveDto), (SortNode)node);

            var sorted = func.Function(dtos.AsQueryable());

            var expected = new[] { 2, 3, 4 };
            var actual = sorted
                .OfType<PrimitiveDto>()
                .Select(s => s.IntProperty);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }

        [TestMethod]
        public void Limiting_Sort_Take_More_Than_Available()
        {
            var node = QueryNode.Sort(
                new[] { QueryNode.Ascending(QueryNode.Member("IntProperty")) },
                QueryNode.Limit(QueryNode.Literal(0), QueryNode.Literal(100)));

            var func = Statics.SortGenerator.Generate(typeof(PrimitiveDto), (SortNode)node);

            var sorted = func.Function(dtos.AsQueryable());

            var expected = new[] { 1, 2, 3, 4 };
            var actual = sorted
                .OfType<PrimitiveDto>()
                .Select(s => s.IntProperty);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }

        [TestMethod]
        public void Limiting_Sort_Take_None()
        {
            var node = QueryNode.Sort(
                new[] { QueryNode.Ascending(QueryNode.Member("IntProperty")) },
                QueryNode.Limit(
                    QueryNode.Literal(-1), 
                    QueryNode.Literal(-1)));

            var func = Statics.SortGenerator.Generate(typeof(PrimitiveDto), (SortNode)node);

            var sorted = func.Function(dtos.AsQueryable());

            var expected = new int[0];
            var actual = sorted
                .OfType<PrimitiveDto>()
                .Select(s => s.IntProperty);

            Assert.IsTrue(expected.SequenceEqual(actual));
        }
    }
}