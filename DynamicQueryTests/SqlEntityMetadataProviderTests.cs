﻿using System.Linq;
using DynamicQuery.Data.EntityFramework;
using DynamicQuery.Data.SqlServer;
using DynamicQuery.Data.Types;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DynamicQueryTests
{
    [TestClass]
    public class SqlEntityMetadataProviderTests
    {
        [TestMethod]
        public void Integration__Can_Query_Metadata_From_Sql()
        {
            var provider = new SqlEntityMetadataProvider(
                @"Data Source=.\sqlexpress;Initial Catalog=DynamicDataMock;Integrated Security=True",
                "CustomField_%");
            
            Assert.IsTrue(provider.GetMetadata().Any());
        }

        [TestMethod]
        public void Integration__Can_Generate_Runtime_DbSet()
        {
            var assemblyBuilder = new DynamicAssemblyBuilder(Random.FriendlyName());
            var metaDataProvider = new SqlEntityMetadataProvider(Settings.SqlConnection, "CustomFields_%");
            var contextFactory = new DefaultDbContextFactory(Settings.SqlConnection, assemblyBuilder, metaDataProvider);

            var tableName = SqlTable.Create("CustomFields_", 5);
            var context = contextFactory.Create();

            var customType = contextFactory.CustomFields[tableName];
            var set = context.Set(customType);
            var count = set.Compile().Count();

            SqlTable.Delete(tableName);

            Assert.AreEqual(5, count);
        }
    }
}