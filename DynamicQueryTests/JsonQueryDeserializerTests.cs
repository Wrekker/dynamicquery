﻿using System;
using System.IO;
using System.Linq;
using DynamicQuery.Query.Parsing;
using DynamicQuery.Query.Syntax;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;

namespace DynamicQueryTests
{
    [TestClass]
    public class JsonQueryDeserializerTests
    {
        private PrimitiveDto[] dtos =
        {
            new PrimitiveDto {IntProperty = 1},
            new PrimitiveDto {IntProperty = 2},
            new PrimitiveDto {IntProperty = 3}
        };

        [TestMethod]
        public void Deserialize_Json_Filter()
        {
            var json = @"[ ""filter"", [ "">="", 2, [ ""member"", ""IntProperty"" ] ] ]";
            var converter = new JsonQueryDeserializer();

            var jsonReader = new JsonTextReader(new StringReader(json));

            var node = converter.Deserialize(jsonReader) as FilterNode;

            var where = Statics.FilterGenerator.Generate(typeof(PrimitiveDto), node);

            Assert.AreEqual(
                2,
                where.Function(dtos.AsQueryable())
                    .OfType<PrimitiveDto>()
                    .Count());
        }

        [TestMethod]
        public void Deserialize_Json_Sort()
        {
            var json = @"
                [ ""sort"", 
                    [ ""desc"", [ ""member"", ""IntProperty"" ] ] 
                ]";
            var converter = new JsonQueryDeserializer();

            var jsonReader = new JsonTextReader(new StringReader(json));

            var node = converter.Deserialize(jsonReader) as SortNode;

            var sort = Statics.SortGenerator.Generate(typeof(PrimitiveDto), node);

            var result = sort.Function(dtos.AsQueryable()).OfType<PrimitiveDto>();

            Assert.AreEqual(
                3,
                result.First().IntProperty);
        }

        [TestMethod]
        public void Deserialize_Json_Map()
        {
            var json = @"[ ""select"", [ ""member"", ""IntProperty"" ] ]";
            var converter = new JsonQueryDeserializer();

            var jsonReader = new JsonTextReader(new StringReader(json));

            var node = converter.Deserialize(jsonReader) as SelectNode;

            var map = Statics.SelectGenerator.Generate(typeof(PrimitiveDto), node);

            var result = map.Function(dtos.AsQueryable())
                .Cast<object>();

            var type = result.First().GetType();

            Assert.IsNull(type.GetProperty("StringProperty"));

            Assert.AreEqual(
                1,
                type.GetProperty("IntProperty").GetValue(result.First()));
        }

        [TestMethod]
        public void Deserialize_Json_Pipe()
        {
            var json = @"
                [ ""pipe"", 
                    [ ""filter"", [ "">="", [ ""member"", ""IntProperty"" ], 3 ] ],
                    [ ""filter"", [ ""<="", [ ""member"", ""IntProperty"" ], 3 ] ]
                ]";

            var converter = new JsonQueryDeserializer();
            var jsonReader = new JsonTextReader(new StringReader(json));
            var node = converter.Deserialize(jsonReader) as PipeNode;
            var filters = Statics.PipingGenerator.Generate(typeof(PrimitiveDto), node);

            var result = filters.Function(dtos.AsQueryable())
                .Cast<object>();

            Assert.AreEqual(1, result.Count());
        }
    }
}