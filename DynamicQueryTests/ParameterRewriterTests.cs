﻿using System;
using System.Linq.Expressions;
using DynamicQuery.Query.Generation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DynamicQueryTests
{
    [TestClass]
    public class ParameterRewriterTests
    {
        [TestMethod]
        public void Ensure_Monadic_ParameterRewriter_Replaces_In_Body()
        {
            var expected = 30;
            var expr = ((Expression<Func<int, int>>)(b => b));
            var replacements = new Expression[]
            {
                Expression.Constant(expected)
            };

            var rewriter = new ParameterRewriter(expr.Parameters, replacements);
            var rewrittenBody = rewriter.Visit(expr.Body);

            var newLambda = Expression.Lambda<Func<int, int>>(rewrittenBody, expr.Parameters);

            var actual = newLambda.Compile()(1);

            // Should now be a => 30, ignoring the parameter.
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void Ensure_Polyadic_ParameterRewriter_Replaces_In_Body()
        {
            var expected = 30 - 70;
            var expr = ((Expression<Func<int, int, int>>)((a, b) => a - b));
            var replacements = new Expression[]
            {
                Expression.Constant(30),
                Expression.Constant(70)
            };
            
            var rewriter = new ParameterRewriter(expr.Parameters, replacements);
            var rewrittenBody = rewriter.Visit(expr.Body);

            var newLambda = Expression.Lambda<Func<int, int, int>>(rewrittenBody, expr.Parameters);
            
            var actual = newLambda.Compile()(1, 5);

            // Should now be (a, b) => 30 - 70, ignoring the parameters.
            Assert.AreEqual(actual, expected);
        }
    }
}