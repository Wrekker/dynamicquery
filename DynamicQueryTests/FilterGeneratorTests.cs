﻿using System.Linq;
using DynamicQuery.Query.Syntax;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DynamicQueryTests
{
    [TestClass]
    public class FilterGeneratorTests
    {
        private PrimitiveDto[] dtos =
        {
            new PrimitiveDto {StringProperty = "abc", IntProperty = 1 },
            new PrimitiveDto {StringProperty = "bcd", IntProperty = 1 },
            new PrimitiveDto {StringProperty = "cde", IntProperty = 2 },
            new PrimitiveDto {StringProperty = "str", IntProperty = 2 },
            new PrimitiveDto {StringProperty = "str1", IntProperty = 3  },
            new PrimitiveDto {StringProperty = "str2", IntProperty = 3  },
            new PrimitiveDto {StringProperty = "str3", IntProperty = 4  },
        };

        [TestMethod]
        public void Contains_Matches_Entire_String()
        {
            var expectedCount = 1;

            var query =
                (FilterNode) QueryNode.Filter(
                    QueryNode.Contains(
                        QueryNode.Member("StringProperty"),
                        QueryNode.Literal("abc")));

            var applicator =
                Statics.FilterGenerator.Generate(
                    typeof (PrimitiveDto),
                    query);

            var actualCount = applicator
                .Function(dtos.AsQueryable())
                .Cast<PrimitiveDto>()
                .Count();

            Assert.AreEqual(expectedCount, actualCount);
        }

        [TestMethod]
        public void Contains_Matches_Prefix()
        {
            var expectedCount = 4;

            var query =
                (FilterNode) QueryNode.Filter(
                    QueryNode.Contains(
                        QueryNode.Member("StringProperty"),
                        QueryNode.Literal("str")));

            var applicator =
                Statics.FilterGenerator.Generate(
                    typeof(PrimitiveDto),
                    query);

            var actualCount = applicator
                .Function(dtos.AsQueryable())
                .Cast<PrimitiveDto>()
                .Count();

            Assert.AreEqual(expectedCount, actualCount);
        }

        [TestMethod]
        public void Contains_Matches_Substring()
        {
            var expectedCount = 3;

            var query =
                (FilterNode) QueryNode.Filter(
                    QueryNode.Contains(
                        QueryNode.Member("StringProperty"),
                        QueryNode.Literal("c")));

            var applicator =
                Statics.FilterGenerator.Generate(
                    typeof(PrimitiveDto),
                    query);

            var actualCount = applicator
                .Function(dtos.AsQueryable())
                .Cast<PrimitiveDto>()
                .Count();

            Assert.AreEqual(expectedCount, actualCount);
        }

        [TestMethod]
        public void Equal_Matches_String_Exactly()
        {
            var expectedCount = 1;

            var query =
                (FilterNode) QueryNode.Filter(
                    QueryNode.Equal(
                        QueryNode.Member("StringProperty"),
                        QueryNode.Literal("str")));

            var applicator =
                Statics.FilterGenerator.Generate(
                    typeof(PrimitiveDto),
                    query);

            var actualCount = applicator
                .Function(dtos.AsQueryable())
                .Cast<PrimitiveDto>()
                .Count();

            Assert.AreEqual(expectedCount, actualCount);
        }

        [TestMethod]
        public void NotEqual_Excludes_Exactly()
        {
            var expectedCount = 6;

            var query =
                (FilterNode) QueryNode.Filter(
                    QueryNode.NotEqual(
                        QueryNode.Member("StringProperty"),
                        QueryNode.Literal("str")));

            var applicator =
                Statics.FilterGenerator.Generate(
                    typeof (PrimitiveDto),
                    query);

            var actualCount = applicator
                .Function(dtos.AsQueryable())
                .Cast<PrimitiveDto>()
                .Count();

            Assert.AreEqual(expectedCount, actualCount);
        }

        [TestMethod]
        public void LessThan_Is_Not_Upper_Inclusive()
        {
            var expectedCount = 4;

            var query =
                (FilterNode) QueryNode.Filter(
                    QueryNode.LessThan(
                        QueryNode.Member("IntProperty"),
                        QueryNode.Literal(3)));

            var applicator =
                Statics.FilterGenerator.Generate(
                    typeof (PrimitiveDto),
                    query);

            var actualCount = applicator
                .Function(dtos.AsQueryable())
                .Cast<PrimitiveDto>()
                .Count();

            Assert.AreEqual(expectedCount, actualCount);
        }

        [TestMethod]
        public void Between_Is_Lower_And_Upper_Inclusive()
        {
            var expectedCount = 4;

            var query =
                (FilterNode)QueryNode.Filter(
                    QueryNode.Between(
                        QueryNode.Member("IntProperty"),
                        QueryNode.Literal(2),
                        QueryNode.Literal(3)));

            var applicator =
                Statics.FilterGenerator.Generate(
                    typeof(PrimitiveDto),
                    query);

            var actualCount = applicator
                .Function(dtos.AsQueryable())
                .Cast<PrimitiveDto>()
                .Count();

            Assert.AreEqual(expectedCount, actualCount);
        }
    }
}
