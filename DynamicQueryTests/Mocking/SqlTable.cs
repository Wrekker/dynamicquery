﻿using System.Data.SqlClient;
using DynamicQuery.Data.SqlServer;

namespace DynamicQueryTests
{
    public class SqlTable
    {
        private static readonly string _createTable =
            @"create table {0} (
                Id int identity(1,1) primary key not null,
                Name nvarchar(100) not null,
                CreatedOn datetime null);";

        private static readonly string _insertIntoTable =
            @"insert into {0} (Name, CreatedOn) 
                values (@name, @createdOn)";

        private static readonly string _deleteTable =
            @"drop table {0};";

        public static string Create(string prefix, int rowsToInsert = 0)
        {
            using (var connection = new SqlReader(Settings.SqlConnection))
            {
                var name = Random.FriendlyName();
                var createSql = string.Format(_createTable, prefix + name);

                connection.Execute(createSql);

                var insertSql = string.Format(_insertIntoTable, prefix + name);
                for (int i = 0; i < rowsToInsert; i++)
                {
                    connection.Execute(insertSql, 
                        new SqlParameter("name", Random.FriendlyName()),
                        new SqlParameter("createdOn", Random.FriendlyDateTime()));
                }

                return prefix + name;
            }
        }

        public static void Delete(string name)
        {
            using (var connection = new SqlReader(Settings.SqlConnection))
            {
                var sql = string.Format(_deleteTable, name);

                connection.Execute(sql);
            }
        }
    }
}