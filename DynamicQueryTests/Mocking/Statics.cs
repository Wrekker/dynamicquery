﻿using DynamicQuery.Data.Types;
using DynamicQuery.Query.Generation;

namespace DynamicQueryTests
{
    internal class Statics
    {
        private static IDynamicAssemblyBuilder assemblyBuilder = new DynamicAssemblyBuilder("GeneratorAssembly");

        public static TerminalGenerator TerminalGenerator = new TerminalGenerator();
        public static LogicalGenerator LogicalGenerator = new LogicalGenerator(TerminalGenerator);
        public static AggregatorGenerator AggregatorGenerator = new AggregatorGenerator();

        public static FilterGenerator FilterGenerator = new FilterGenerator(LogicalGenerator);
        public static AggregationGenerator AggregationGenerator = new AggregationGenerator(AggregatorGenerator, assemblyBuilder);
        public static SelectGenerator SelectGenerator = new SelectGenerator(TerminalGenerator, assemblyBuilder);
        public static SortGenerator SortGenerator = new SortGenerator(TerminalGenerator);

        public static PipingGenerator PipingGenerator = new PipingGenerator(
            FilterGenerator, 
            AggregationGenerator,
            SelectGenerator,
            SortGenerator);

        public static QueryGenerator QueryGenerator = new QueryGenerator(
            FilterGenerator,
            AggregationGenerator,
            SelectGenerator,
            SortGenerator,
            PipingGenerator);
    }
}
