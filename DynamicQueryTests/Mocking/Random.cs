﻿using System;
using System.Linq;

namespace DynamicQueryTests
{
    public static class Random
    {
        private static readonly System.Random Rnd = new System.Random();

        public static string FriendlyName()
        {
            return new string(
                new char[10]
                    .Select(s => (char)Rnd.Next('a', 'z' + 1))
                    .ToArray());
        }

        public static int FriendlyInt()
        {
            return Rnd.Next(0, 100000);
        }

        public static DateTime FriendlyDateTime()
        {
            return DateTime.UtcNow.AddHours(Rnd.Next(-10000, 10001));
        }
    }
}