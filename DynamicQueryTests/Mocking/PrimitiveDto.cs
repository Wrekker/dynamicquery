﻿using System;

namespace DynamicQueryTests
{
    public class PrimitiveDto
    {
        public DateTime DateProperty { get; set; }
        public double DoubleProperty { get; set; }
        public string StringProperty { get; set; }
        public int IntProperty { get; set; }
    }
}