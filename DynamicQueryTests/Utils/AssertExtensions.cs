﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DynamicQueryTests.Utils
{
    public static class AssertUtil
    {
        public static void Throws<T>(Action call)
            where T : Exception
        {
            try { call(); }
            catch (T) { }
        }


    }
}