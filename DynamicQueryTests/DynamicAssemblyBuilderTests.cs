﻿using System;
using DynamicQuery.Data.Types;
using DynamicQuery.Types;
using DynamicQueryTests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DynamicQueryTests
{
    [TestClass]
    public class DynamicAssemblyBuilderTests
    {
        [TestMethod]
        public void Verify_AutoProperty_Get()
        {
            var builder = new DynamicAssemblyBuilder(Random.FriendlyName());
            var type = builder.CreateType(t => t
                .Field(typeof (int), "TestProperty", f => f)
                .WithDefaultConstructor());
            
            var instance = Activator.CreateInstance(type);

            var zero = (int)type.GetProperty("TestProperty").GetValue(instance);
            
            Assert.AreEqual(0, zero);
        }

        [TestMethod]
        public void Verify_AutoProperty_Set()
        {
            var builder = new DynamicAssemblyBuilder(Random.FriendlyName());
            var type = builder.CreateType(t => t
                .Field(typeof (int), "TestProperty", f => f
                    .IsWritable())
                .WithDefaultConstructor());

            var instance = Activator.CreateInstance(type);

            type.GetProperty("TestProperty").SetValue(instance, 10);
            var ten = type.GetProperty("TestProperty").GetValue(instance);

            Assert.AreEqual(10, ten);
        }

        [TestMethod]
        public void Verify_AutoProperty_Set_Multiple()
        {
            var builder = new DynamicAssemblyBuilder(Random.FriendlyName());
            var type = builder.CreateType(t => t
                .Field(typeof (int), "TestInt", f => f
                    .IsWritable())
                .Field(typeof (string), "TestString", f => f
                    .IsWritable())
                .WithDefaultConstructor());

            var instance = Activator.CreateInstance(type);

            type.GetProperty("TestInt").SetValue(instance, 10);
            var ten = type.GetProperty("TestInt").GetValue(instance);

            type.GetProperty("TestString").SetValue(instance, "Abc");
            var abc = type.GetProperty("TestString").GetValue(instance);

            Assert.AreEqual(10, ten);
            Assert.AreEqual("Abc", abc);
        }

        [TestMethod]
        public void Verify_AutoProperty_With_No_Setter_Via_Initialization_Constructor()
        {
            var builder = new DynamicAssemblyBuilder(Random.FriendlyName());
            var type = builder.CreateType(t => t
                .Field(typeof(int), "TestProperty", f => f)
                .WithInitializationConstructor());

            var instance = Activator.CreateInstance(type, new object[] { 10 }, null);
            
            var ten = type.GetProperty("TestProperty").GetValue(instance);
            
            AssertUtil.Throws<ArgumentException>(() => instance.SetDynamically("TestProperty", 123));                                         
            Assert.AreEqual(10, ten);
        }

        [TestMethod]
        public void Verify_Equals()
        {
            var builder = new DynamicAssemblyBuilder(Random.FriendlyName());
            var type = builder.CreateType(t => t
                .Field(typeof(int), "TestInt", f => f
                   .IsWritable()
                   .IncludeInEquality())
                .Field(typeof(string), "TestString", f => f
                   .IsWritable()
                   .IncludeInEquality())
                .WithInitializationConstructor());

            var instanceA = Activator.CreateInstance(type, new object[] { 1, "a" }, null);
            var instanceA2 = Activator.CreateInstance(type, new object[] { 1, "a" }, null);
            var instanceB = Activator.CreateInstance(type, new object[] { 1, "b" }, null);
            
            Assert.AreEqual(instanceA, instanceA2);
            Assert.AreEqual(instanceA2, instanceA);
            Assert.AreEqual(instanceB, instanceB);

            Assert.AreNotEqual(instanceA, instanceB);
            Assert.AreNotEqual(instanceA, null);
        }

        [TestMethod]
        public void Verify_GetHashcode()
        {
            var builder = new DynamicAssemblyBuilder(Random.FriendlyName());
            var type = builder.CreateType(t => t
                .Field(typeof(int), "TestInt", f => f
                   .IsWritable()
                   .IncludeInEquality())
                .Field(typeof(string), "TestString", f => f
                   .IsWritable()
                   .IncludeInEquality())
                .WithInitializationConstructor());

            var instanceA = Activator.CreateInstance(type, new object[] { 1, "a" }, null);
            var instanceA2 = Activator.CreateInstance(type, new object[] { 1, "a" }, null);
            var instanceB = Activator.CreateInstance(type, new object[] { 1, null }, null);

            Assert.AreEqual(instanceA.GetHashCode(), instanceA2.GetHashCode());
            Assert.AreEqual(instanceA2.GetHashCode(), instanceA.GetHashCode());
            
            // This isn't necessarily generally true - if the hashing algorithm changes 
            // we might get a collision here.
            Assert.AreNotEqual(instanceA.GetHashCode(), instanceB.GetHashCode());
        }

        [TestMethod]
        public void Verify_ToString()
        {
            var builder = new DynamicAssemblyBuilder(Random.FriendlyName());
            var type = builder.CreateType(t => t
                .Field(typeof(int), "TestInt", f => f
                   .IsWritable())
                .Field(typeof(string), "TestString", f => f
                   .IsWritable())
                .WithDefaultConstructor());

            var instance = Activator.CreateInstance(type);

            var testInteger = 5;
            var testString = "Test";

            instance.SetDynamically("TestInt", testInteger);
            instance.SetDynamically("TestString", testString);

            var expected = string.Format("{{ TestInt = {0}, TestString = {1} }}", testInteger, testString);

            Assert.AreEqual(expected, instance.ToString());
        }

    }
}