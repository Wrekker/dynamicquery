﻿using System.Linq;
using DynamicQuery.Data.EntityFramework;
using DynamicQuery.Data.Splicing;
using DynamicQuery.Data.SqlServer;
using DynamicQuery.Data.Types;
using DynamicQuery.Query.Syntax;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DynamicQueryTests
{
    [TestClass]
    public class JoinerTests
    {
        [TestMethod]
        public void Can_Generate_GroupJoin()
        {
            var assemblyBuilder = new DynamicAssemblyBuilder(Random.FriendlyName());
            var joiner = new Joiner(assemblyBuilder);

            var inner = new[]
            {
                new PrimitiveDto {IntProperty = 1, StringProperty = Random.FriendlyName()},
                new PrimitiveDto {IntProperty = 2, StringProperty = Random.FriendlyName()},
                new PrimitiveDto {IntProperty = 3, StringProperty = Random.FriendlyName()},
            };
            var outer = new[]
            {
                new PrimitiveDto {IntProperty = 1, StringProperty = Random.FriendlyName()},
                new PrimitiveDto {IntProperty = 2, StringProperty = Random.FriendlyName()},
                new PrimitiveDto {IntProperty = 2, StringProperty = Random.FriendlyName()},
                new PrimitiveDto {IntProperty = 3, StringProperty = Random.FriendlyName()},
                new PrimitiveDto {IntProperty = 3, StringProperty = Random.FriendlyName()},
                new PrimitiveDto {IntProperty = 3, StringProperty = Random.FriendlyName()},
            };

            var joined = joiner.Join(
                inner.AsQueryable(),
                outer.AsQueryable(),
                new BasicJoinDescriptor(
                    typeof (PrimitiveDto),
                    typeof (PrimitiveDto),
                    new JoinRelation("IntProperty", "IntProperty")));

            var count = Statics.FilterGenerator.Generate(
                joined.ElementType,
                (FilterNode)QueryNode.Filter(
                    QueryNode.Equal(
                        QueryNode.Member("Outer_PrimitiveDto_IntProperty"),
                        QueryNode.Literal(3)))).Function(joined)
                .Cast<object>();

            Assert.AreEqual(3, count.Count());
        }

        [TestMethod]
        public void Can_Generate_GroupJoin_With_EmptyOuter()
        {
            var assemblyBuilder = new DynamicAssemblyBuilder(Random.FriendlyName());
            var joiner = new Joiner(assemblyBuilder);

            var inner = new[]
            {
                new PrimitiveDto {IntProperty = 1, StringProperty = Random.FriendlyName()},
                new PrimitiveDto {IntProperty = 2, StringProperty = Random.FriendlyName()},
                new PrimitiveDto {IntProperty = 3, StringProperty = Random.FriendlyName()},
            };
            var outer = new[]
            {
                new PrimitiveDto {IntProperty = 4, StringProperty = Random.FriendlyName()},
                new PrimitiveDto {IntProperty = 5, StringProperty = Random.FriendlyName()},
                new PrimitiveDto {IntProperty = 6, StringProperty = Random.FriendlyName()},
            };

            var joined = joiner.Join(
                inner.AsQueryable(),
                outer.AsQueryable(),
                new BasicJoinDescriptor(
                    typeof(PrimitiveDto),
                    typeof(PrimitiveDto),
                    new JoinRelation("IntProperty", "IntProperty")));
        }

        [TestMethod]
        public void Integration__Join_Non_Matching_Tables()
        {
            var assemblyBuilder = new DynamicAssemblyBuilder(Random.FriendlyName());
            var metaDataProvider = new SqlEntityMetadataProvider(Settings.SqlConnection, "CustomFields_%");
            var contextFactory = new DefaultDbContextFactory(Settings.SqlConnection, assemblyBuilder, metaDataProvider);

            var tableName1 = SqlTable.Create("CustomFields_", 5);
            var tableName2 = SqlTable.Create("CustomFields_", 3);
            var context = contextFactory.Create();

            var customType1 = contextFactory.CustomFields[tableName1];
            var customType2 = contextFactory.CustomFields[tableName2];
            var set1 = context.Set(customType1);
            var set2 = context.Set(customType2);
            
            var joiner = new Joiner(assemblyBuilder);
            
            var joined = joiner.Join(
                set1,
                set2,
                new BasicJoinDescriptor(
                    customType1,
                    customType2,
                    new JoinRelation("Id", "Id")))
                .Compile();

            Assert.AreEqual(5, joined.Count());

            SqlTable.Delete(tableName1);
            SqlTable.Delete(tableName2);
        }

        [TestMethod]
        public void Integration__Join_Three_Tables()
        {
            var assemblyBuilder = new DynamicAssemblyBuilder(Random.FriendlyName());
            var metaDataProvider = new SqlEntityMetadataProvider(Settings.SqlConnection, "CustomFields_%");
            var contextFactory = new DefaultDbContextFactory(Settings.SqlConnection, assemblyBuilder, metaDataProvider);

            var tableName1 = SqlTable.Create("CustomFields_Outer_", 5);
            var tableName2 = SqlTable.Create("CustomFields_Inner1_", 3);
            var tableName3 = SqlTable.Create("CustomFields_Inner2_", 3);

            var context = contextFactory.Create();

            var customType1 = contextFactory.CustomFields[tableName1];
            var customType2 = contextFactory.CustomFields[tableName2];
            var customType3 = contextFactory.CustomFields[tableName3];
            var set1 = context.Set(customType1);
            var set2 = context.Set(customType2);
            var set3 = context.Set(customType3);

            var joiner = new Joiner(assemblyBuilder);

            var joined1 = joiner.Join(
                set1,
                set2,
                new BasicJoinDescriptor(
                    customType1,
                    customType2,
                    new JoinRelation("Id", "Id")));

            var joined2 = joiner.Join(
                joined1,
                set3,
                new BasicJoinDescriptor(
                    joined1.ElementType,
                    customType3,
                    new JoinRelation("Outer_" + customType1.Name + "_Id", "Id")))
                .Compile();

            Assert.AreEqual(5, joined2.Count());

            SqlTable.Delete(tableName1);
            SqlTable.Delete(tableName2);
            SqlTable.Delete(tableName3);
        }
    }
}