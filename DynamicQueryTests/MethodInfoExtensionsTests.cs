﻿using System;
using DynamicQuery.Types;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DynamicQueryTests
{
    [TestClass]
    public class MethodInfoExtensionsTests
    {
        [TestMethod]
        public void Ensure_SetDynamically_Sets_Value()
        {
            var number = Random.FriendlyInt();
            var dto = new PrimitiveDto();
            
            dto.SetDynamically("IntProperty", number);
            
            Assert.AreEqual(number, dto.IntProperty);
        }

        [TestMethod]
        public void Ensure_GetDynamically_Gets_Value()
        {
            var number = Random.FriendlyInt();
            var dto = new PrimitiveDto { IntProperty = number };

            var result = dto.GetDynamically<int>("IntProperty");

            Assert.AreEqual(number, result);
        }

        [TestMethod]
        public void Verify_InvokeStatic()
        {
            var methodInfo = typeof(int).GetMethod("Parse", new[] { typeof(string) });

            var result = methodInfo.InvokeStatic<int>("5");
            var expected = int.Parse("5");

            Assert.AreEqual(expected, result);
        }
    }
}