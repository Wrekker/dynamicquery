﻿using System.Linq;
using DynamicQuery.Query.Syntax;
using DynamicQuery.Types;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DynamicQueryTests
{
    [TestClass]
    public class PipeGeneratorTests
    {
        private PrimitiveDto[] dtos =
        {
            new PrimitiveDto { IntProperty = 5, DoubleProperty = 1 },
            new PrimitiveDto { IntProperty = 4, DoubleProperty = 1 },
            new PrimitiveDto { IntProperty = 3, DoubleProperty = 1 },
            new PrimitiveDto { IntProperty = 2, DoubleProperty = 1 },
            new PrimitiveDto { IntProperty = 1, DoubleProperty = 1 }
        };

        [TestMethod]
        public void Ensure_Piping_Sequences_Correctly()
        {
            var query =
                (PipeNode) QueryNode.Pipe(
                    new[]
                    {
                        QueryNode.Filter(
                            QueryNode.LessThan(
                                QueryNode.Member("IntProperty"),
                                QueryNode.Literal(5))),
                        QueryNode.Aggregate(
                            QueryNode.Over(new[] {QueryNode.Member("DoubleProperty")}),
                            new[] {QueryNode.Sum(QueryNode.Member("IntProperty"))}),
                        QueryNode.Filter(
                            QueryNode.Equal(
                                QueryNode.Member("IntProperty"),
                                QueryNode.Literal(1 + 2 + 3 + 4))),
                    });

            var applicator = Statics.PipingGenerator.Generate(
                typeof (PrimitiveDto),
                query);

            var result = applicator.Function(dtos.AsQueryable())
                .Cast<object>()
                .First();

            Assert.AreEqual(
                1 + 2 + 3 + 4,
                result.GetDynamically<int>("IntProperty"));
        }
    }
}