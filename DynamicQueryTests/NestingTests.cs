﻿using System.Linq;
using DynamicQuery.Query.Syntax;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DynamicQueryTests
{
    [TestClass]
    public class NestingTests
    {
        private PrimitiveDto[] dtos =
        {
            new PrimitiveDto { StringProperty = "1 no" },
            new PrimitiveDto { StringProperty = "2 yes" },
            new PrimitiveDto { StringProperty = "3 yes" },
            new PrimitiveDto { StringProperty = "4 no" },
            new PrimitiveDto { StringProperty = "5 yes" }
        };

        [TestMethod]
        public void Sort_Filtered_List()
        {
            var filter = Statics.FilterGenerator.Generate(
                typeof (PrimitiveDto),
                (FilterNode) QueryNode.Filter(
                    QueryNode.Contains(
                        QueryNode.Member("StringProperty"),
                        QueryNode.Literal("yes"))));
            

            var sort = Statics.SortGenerator.Generate(
                typeof (PrimitiveDto),
                (SortNode) QueryNode.Sort(new []
                {
                    QueryNode.Ascending(QueryNode.Member("IntProperty")),
                }));


            var result = sort.Function(filter.Function(dtos.AsQueryable()))
                .OfType<PrimitiveDto>();

            Assert.AreEqual(
                "2 yes",
                result.First().StringProperty);

            Assert.AreEqual(
                "5 yes",
                result.Last().StringProperty);
        }

        [TestMethod]
        public void Filter_Sorted_List()
        {
            var filter = Statics.FilterGenerator.Generate(
                typeof(PrimitiveDto),
                (FilterNode) QueryNode.Filter(
                    QueryNode.Contains(
                        QueryNode.Member("StringProperty"),
                        QueryNode.Literal("yes"))));


            var sort = Statics.SortGenerator.Generate(
                typeof (PrimitiveDto),
                (SortNode) QueryNode.Sort(new []
                {
                    QueryNode.Ascending(QueryNode.Member("IntProperty"))
                }));


            var result = filter.Function(sort.Function(dtos.AsQueryable()))
                .OfType<PrimitiveDto>();

            Assert.AreEqual(
                "2 yes",
                result.First().StringProperty);

            Assert.AreEqual(
                "5 yes",
                result.Last().StringProperty);
        }
    }
}