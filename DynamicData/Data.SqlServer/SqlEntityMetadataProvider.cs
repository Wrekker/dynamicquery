using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using DynamicData.Data.EntityFramework;

namespace DynamicData.Data.SqlServer
{
    internal class SqlEntityMetadataProvider : IEntityMetadataProvider
    {
        private const string Query =
            @"select 
	            t.name as [Table], 
	            c.name as [Column], 
	            y.name as [Type], 
	            c.is_nullable as [IsNullable],
	            (case when ic.index_id != 0 then 1 else 0 end) as IsInKey
	            from sys.tables t
		            inner join sys.columns c
			            on t.object_id = c.object_id
		            inner join sys.types y
			            on c.system_type_id = y.system_type_id
			            and c.user_type_id = y.user_type_id
		            inner join sys.indexes i
			            on t.object_id = i.object_id
		            left join sys.index_columns ic
			            on t.object_id = ic.object_id
			            and i.index_id = ic.index_id
			            and c.column_id = ic.column_id
	            where t.name like 'CustomFields_%'
		            and i.is_primary_key = 1		
	            order by c.column_id";

        private readonly string _connectionString;
        private readonly string _tablePattern;

        public SqlEntityMetadataProvider(string connectionString, string tablePattern)
        {
            _connectionString = connectionString;
            _tablePattern = tablePattern;
        }

        public IReadOnlyList<EntityMetadata> GetMetadata()
        {
            using (var connection = new Connection(_connectionString))
            {
                var reader = connection.Read(
                    Query, 
                    dr => new
                    {
                        Table = dr["Table"] as string,
                        Column = dr["Column"] as string,
                        Type = dr["Type"] as string,
                        IsNullable = dr["IsNullable"] as bool? ?? false,
                    },
                    new SqlParameter("pattern", _tablePattern));

                return reader
                    .GroupBy(g => g.Table)
                    .Select(s => new EntityMetadata
                    {
                        EntityName = s.Key,
                        Fields = s
                            .Select(e => new EntityField(ToClrType(e.Type, e.IsNullable), e.Column))
                            .ToArray()
                    })
                    .ToArray();
            }
        }

        private Type ToClrType(string sqlType, bool isNullable)
        {
            switch (sqlType)
            {
                case "int": return typeof(int);
                case "datetime": return typeof(DateTime);
                case "nvarchar": return typeof(string);
            }

            throw new NotSupportedException("SqlType not supported.");
        }
    }
}