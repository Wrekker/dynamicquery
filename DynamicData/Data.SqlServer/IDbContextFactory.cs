﻿using System.Data.Entity;

namespace DynamicData.Data.SqlServer
{
    public interface IDbContextFactory<out T>
        where T : DbContext
    {
        T CreateContext();
    }
}