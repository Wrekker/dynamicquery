using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;

namespace DynamicData.Data.Types
{
    internal class DynamicAssemblyBuilder : IDynamicAssemblyBuilder
    {
        private readonly Guid _guid = Guid.NewGuid();
        private readonly IList<IDynamicTypeBuilder> _typeBuilders = new List<IDynamicTypeBuilder>(); 
        
        public IDynamicAssemblyBuilder Type(string name, Func<IDynamicTypeBuilder, IDynamicTypeBuilder> builder)
        {
            var typeName = string.Format("Dynamic.{0}.Assembly.Module.{1}", _guid, name);
            _typeBuilders.Add(builder(new DynamicTypeBuilder(typeName)));

            return this;
        }

        public Assembly Build()
        {
            var assemblyName = string.Format("Dynamic.{0}.Assembly", _guid);
            var moduleName = string.Format("Dynamic.{0}.Assembly.Module", _guid);
            
            var assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(new AssemblyName(assemblyName), AssemblyBuilderAccess.Run);
            var moduleBuilder = assemblyBuilder.DefineDynamicModule(moduleName);

            foreach (var typeBuilder in _typeBuilders)
            {
                typeBuilder.Build(moduleBuilder);
            }

            return assemblyBuilder;
        }
    }
}