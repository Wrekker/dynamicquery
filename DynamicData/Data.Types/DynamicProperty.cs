using System.Reflection.Emit;

namespace DynamicData.Data.Types
{
    internal class DynamicProperty
    {
        public FieldBuilder FieldBuilder { get; set; }
        public PropertyBuilder PropertyBuilder { get; set; }
        public bool IncludedInEquality { get; set; }
    }
}