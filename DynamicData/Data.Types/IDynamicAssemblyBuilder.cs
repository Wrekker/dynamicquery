using System;

namespace DynamicData.Data.Types
{
    internal interface IDynamicAssemblyBuilder
    {
        IDynamicAssemblyBuilder Type(string name, Func<IDynamicTypeBuilder, IDynamicTypeBuilder> builder);
    }
}