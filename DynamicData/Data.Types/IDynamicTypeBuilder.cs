using System;
using System.Reflection.Emit;

namespace DynamicData.Data.Types
{
    internal interface IDynamicTypeBuilder
    {
        string TypeName { get; }
        IDynamicTypeBuilder Implements<T>();
        IDynamicTypeBuilder Field(Type fieldType, string name, Func<IPropertyBuilder, IPropertyBuilder> builder = null);
        IDynamicTypeBuilder WithDefaultConstructor();
        Type Build(ModuleBuilder module);
    }
}