using System;
using System.Reflection.Emit;

namespace DynamicData.Data.Types
{
    internal interface IPropertyBuilder
    {
        Type FieldType { get; }
        string Name { get; }

        IPropertyBuilder IsWritable();
        IPropertyBuilder IncludeInEquality();
        IPropertyBuilder Attribute(Type attributeType, params object[] parameters);

        DynamicProperty Build(TypeBuilder typeBuilder);
    }
}