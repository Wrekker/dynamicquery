﻿using System.Reflection;
using DynamicData.Data.Types;

namespace DynamicData.Data.EntityFramework
{
    public class EntityTypeConfigurationGenerator
    {
        private readonly IEntityMetadataProvider _provider;

        public EntityTypeConfigurationGenerator(IEntityMetadataProvider provider)
        {
            _provider = provider;
        }

        public Assembly GenerateAssembly()
        {
            var metadata = _provider.GetMetadata();

            var assemblyBuilder = new DynamicAssemblyBuilder();

            foreach (var md in metadata)
            {
                assemblyBuilder.Type(md.EntityName, b =>
                {
                    b.WithDefaultConstructor();

                    foreach (var field in md.Fields)
                    {
                        b.Field(field.Type, field.Name, f => f
                            .IsWritable()
                            .IncludeInEquality());
                    }

                    return b;
                });
            }

            return assemblyBuilder.Build();
        }
    }
}