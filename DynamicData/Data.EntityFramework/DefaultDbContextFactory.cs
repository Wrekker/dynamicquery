﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Linq;
using DynamicData.Data.SqlServer;

namespace DynamicData.Data.EntityFramework
{
    public class DefaultDbContextFactory : SqlServer.IDbContextFactory<DbContext>
    {
        private readonly string _connectionString;
        private readonly IDbConnectionFactory _connectionFactory;

        private DateTime _actualVersion = DateTime.UtcNow;
        private DateTime _compiledVersion = DateTime.MinValue;
        private DbCompiledModel _compiledModel;

        public DateTime Version { get { return _compiledVersion; } }

        public Dictionary<string, Type> CustomFields = new Dictionary<string, Type>();

        public DefaultDbContextFactory(string connectionString)
        {
            _connectionString = connectionString;
            _connectionFactory = new SqlConnectionFactory();
        }

        public void Rebuild()
        {
            _actualVersion = DateTime.UtcNow;
            CustomFields.Clear();
        }

        public DbContext CreateContext()
        {
            var connection = _connectionFactory.CreateConnection(_connectionString);

            if (_compiledVersion < _actualVersion)
            {
                _compiledVersion = _actualVersion;
                _compiledModel = CompileModel(connection);
            }

            return new DbContext(connection, _compiledModel, true);
        }

        private DbCompiledModel CompileModel(DbConnection connection)
        {
            var modelBuilder = new DbModelBuilder();
            var provider = new SqlEntityMetadataProvider(
               @"Data Source=.\sqlexpress;Initial Catalog=DynamicDataMock;Integrated Security=True",
               "CustomFields_%");

            var generator = new EntityTypeConfigurationGenerator(provider);

            var assembly = generator.GenerateAssembly();
            
            foreach (var type in assembly.GetTypes())
            {
                CustomFields.Add(type.Name, type);
                var configuration = new DynamicEntityTypeConfiguration(type);

                foreach (var propertyInfo in type.GetProperties())
                {
                    var property = configuration.Property(propertyInfo.Name)
                        .HasColumnName(propertyInfo.Name);
                }

                var addMethod = typeof(ConfigurationRegistrar).GetMethods()
                    .First(f => f.Name == "Add" && f.IsGenericMethod)
                    .MakeGenericMethod(type);

                addMethod.Invoke(modelBuilder.Configurations, new[] { configuration.ToConfiguration() });
            }

            var model = modelBuilder.Build(connection);
            
            return model.Compile();
        }
    }
}