using System.Collections.Generic;

namespace DynamicData.Data.EntityFramework
{
    public class EntityMetadata
    {
        public string EntityName { get; set; }
        public IReadOnlyList<EntityField> Fields { get; set; }
    }
}