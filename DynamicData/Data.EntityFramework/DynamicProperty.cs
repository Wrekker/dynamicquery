using System;

namespace DynamicData.Data.EntityFramework
{
    public class DynamicProperty
    {
        private readonly Type _propertyType;
        private readonly object _property;

        public DynamicProperty(Type propertyType, object property)
        {
            _propertyType = propertyType;
            _property = property;
        }

        public DynamicProperty HasColumnName(string name)
        {
            var method = _property.GetType().GetMethod("HasColumnName");
            method.Invoke(_property, new [] { name });

            return this;
        }
    }
}