﻿using System;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Reflection.Emit;
using DynamicData.Data.Types;

namespace DynamicData.Data.EntityFramework
{
    public class DynamicEntityTypeConfiguration
    {
        private readonly Type _entityType;
        private readonly object _configuration;
        private readonly Type _configurationType;

        public DynamicEntityTypeConfiguration(Type entityType)
        {
            _entityType = entityType;
            _configurationType = typeof (EntityTypeConfiguration<>).MakeGenericType(entityType);
            _configuration = Activator.CreateInstance(_configurationType);
        }

        public DynamicProperty Property(string propertyName)
        {
            var propertyInfo = _entityType.GetProperty(propertyName);
            var propertyParameter = Expression.Parameter(_entityType);
            var propertyLambda = Expression.Lambda(
                Expression.Property(propertyParameter, propertyInfo),
                propertyParameter);
            
            var method = MakePropertyMethod(propertyInfo.PropertyType);
            var property = method.Invoke(_configuration, new[] {propertyLambda});
            
            return new DynamicProperty(propertyInfo.PropertyType, property);
        }

        public object ToConfiguration()
        {
            return _configuration;
        }

        private MethodInfo MakePropertyMethod(Type propertyType)
        {
            if (propertyType == typeof (DateTime)
                || propertyType == typeof(decimal)
                || propertyType == typeof(string)
                || propertyType == typeof(DateTimeOffset)
                || propertyType == typeof(TimeSpan))
            {
                var type = typeof(Expression<>)
                    .MakeGenericType(typeof(Func<,>)
                        .MakeGenericType(_entityType, propertyType));

                return _configurationType.GetMethods()
                    .First(w => w.Name == "Property" && w.GetParameters()[0].ParameterType == type);
            }

            var methods = _configurationType.GetMethods()
                .Where(w => w.Name == "Property" && w.IsGenericMethod);

            var match = methods.First(f =>
            {
                var genericParameter = f.GetGenericArguments()[0];

                var wrappedGeneric = propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>)
                    ? typeof(Nullable<>).MakeGenericType(genericParameter)
                    : genericParameter;
                    
                var type = typeof(Expression<>)
                    .MakeGenericType(typeof(Func<,>)
                        .MakeGenericType(_entityType, wrappedGeneric));

                return f.GetParameters()[0].ParameterType == type;
            });

            return match.MakeGenericMethod(propertyType);
        }
    }
}
