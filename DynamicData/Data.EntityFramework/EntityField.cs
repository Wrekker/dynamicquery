using System;

namespace DynamicData.Data.EntityFramework
{
    public class EntityField
    {
        public string Name { get; private set; }
        public Type Type { get; private set; }

        public EntityField(Type type, string name)
        {
            Type = type;
            Name = name;
        }
    }
}