﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace DynamicData.Data.EntityFramework
{
    public class DynamicDataContext : DbContext
    {
        private readonly DateTime _version;
        private readonly DbModel _model;

        public DynamicDataContext(DateTime version, DbModel model)
        {
            _version = version;
            _model = model;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {


            base.OnModelCreating(modelBuilder);
        }
    }
}
