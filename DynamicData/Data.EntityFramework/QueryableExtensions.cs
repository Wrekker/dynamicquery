﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DynamicData.Data.EntityFramework
{
    public static class QueryableExtensions
    {
        public static IEnumerable<object> Execute(this IQueryable queryable)
        {
            foreach (var q in queryable)
            {
                yield return q;
            }
        }
    }
}