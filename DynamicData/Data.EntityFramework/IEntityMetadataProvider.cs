﻿using System.Collections.Generic;

namespace DynamicData.Data.EntityFramework
{
    public interface IEntityMetadataProvider
    {
        IReadOnlyList<EntityMetadata> GetMetadata();
    }
}